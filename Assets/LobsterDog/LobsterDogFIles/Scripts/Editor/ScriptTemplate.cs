﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

namespace LobsterDog
{
    public class ScriptTemplate : UnityEditor.AssetModificationProcessor
    {
        static string coreProjectName = "LobsterDog";

        public enum ScriptType
        {
            ScriptableObject,
            GUI,
            MonoBehaviour,
            Plugin,
            Editor
        }

        public static void OnWillCreateAsset(string a_path)
        {
            a_path = a_path.Replace(".meta", "");
            int index = a_path.LastIndexOf(".");

            if (index < 0)
            {
                return;
            }

            string file = a_path.Substring(index);

            if (file != ".cs")
            {
                return;
            }

            string fileName = a_path.Substring(a_path.LastIndexOf("/") + 1).Replace(file, "");
            string projectName = a_path.Contains(coreProjectName) ? coreProjectName : PlayerSettings.productName;

            ScriptType scriptType = GetScriptType(a_path);


            index = Application.dataPath.LastIndexOf("Assets");
            a_path = Application.dataPath.Substring(0, index) + a_path;
            file = System.IO.File.ReadAllText(a_path);

            string templateFile = System.IO.File.ReadAllText(EditorApplication.applicationPath.Replace("Unity.exe", "") + "Data\\Resources\\ScriptTemplates\\81-C# Script-NewBehaviourScript.cs.txt");

            if (file.Replace(fileName, "").RemoveWhitespace() != templateFile.Replace("#SCRIPTNAME#", "").Replace("#NOTRIM#", "").Replace("#ROOTNAMESPACEBEGIN#", "").Replace("#ROOTNAMESPACEEND#", "").RemoveWhitespace())
            {
                //"It's an existing script"
                return;
            }

            string header = BuildHeader(projectName);

            string body = BuildUsingBlock(scriptType);

            if (scriptType != ScriptType.Plugin)
            {
                body += Environment.NewLine + "namespace " + projectName.Replace(" ", "");
                body += Environment.NewLine + "{";
            }

            if (scriptType == ScriptType.ScriptableObject)
            {
                body += Environment.NewLine + "\t[CreateAssetMenu(menuName = \"" + projectName + "/" + fileName + "\")]";
            }
            else
            {
                if (scriptType != ScriptType.Editor)
                {
                    string componentMenuPath = "/" + fileName;
                    if (a_path.Contains("Scripts"))
                    {
                        int cutOffIndex = a_path.IndexOf("Scripts") + "Scripts".Length;
                        componentMenuPath = a_path.Substring(cutOffIndex, a_path.Length - cutOffIndex).Replace(".cs", "");
                    }

                    body += Environment.NewLine + "\t[AddComponentMenu(\"" + projectName + componentMenuPath + "\")]";
                }

                if (scriptType == ScriptType.GUI || scriptType == ScriptType.MonoBehaviour)
                {
                    body += Environment.NewLine + "\t[DisallowMultipleComponent]";
                }
            }

            body += Environment.NewLine + (scriptType != ScriptType.Plugin ? "\t" : "") + "public class " + fileName + BuildInheritance(scriptType);


            body += Environment.NewLine + (scriptType != ScriptType.Plugin ? "\t" : "") + "{";

            if (scriptType == ScriptType.GUI || scriptType == ScriptType.MonoBehaviour)
            {
                body += Environment.NewLine + "\t\t#region Reference Fields";
                body += Environment.NewLine + "\t\t//[SerializeField, FoldoutGroup(\"References\")]";
                body += Environment.NewLine + "\t\t#endregion";

                body += Environment.NewLine;

                body += Environment.NewLine + "\t\t#region Fields";
                body += Environment.NewLine + "\t\t#endregion";

                body += Environment.NewLine;

                body += Environment.NewLine + "\t\t#region Properties";
                body += Environment.NewLine + "\t\t#endregion";

                body += Environment.NewLine;

                body += Environment.NewLine + "\t\t#region Methods";
                body += Environment.NewLine + "\t\t#endregion";
            }

            if (scriptType != ScriptType.Plugin)
            {
                body += Environment.NewLine + "\t}";
            }

            body += Environment.NewLine + "}";

            file = header + body;

            System.IO.File.WriteAllText(a_path, file);
            AssetDatabase.Refresh();
        }

        static ScriptType GetScriptType(string a_path)
        {
            if (a_path.Contains("Editor"))
            {
                return ScriptType.Editor;
            }

            if (a_path.Contains("ScriptableObjects"))
            {
                return ScriptType.ScriptableObject;
            }

            if (a_path.Contains("GUI"))
            {
                return ScriptType.GUI;
            }
            
            if (a_path.Contains("Plugins"))
            {
                return ScriptType.Plugin;
            }

            return ScriptType.MonoBehaviour;
        }

        static string BuildHeader(string a_projectName)
        {
            string header = "";
            return header;
        }

        static string BuildUsingBlock(ScriptType a_scriptType)
        {
            if (a_scriptType == ScriptType.Plugin)
            {
                return "";
            }

            string usingBlock = "using System;";
            usingBlock += Environment.NewLine + "using System.Collections;";
            usingBlock += Environment.NewLine + "using System.Collections.Generic;";
            usingBlock += Environment.NewLine + "using UnityEngine;";
            usingBlock += Environment.NewLine + "using " + coreProjectName + ";";

            if (a_scriptType == ScriptType.Editor)
            {
                usingBlock += Environment.NewLine + "using UnityEditor;";
            }
            else
            {
                if (a_scriptType == ScriptType.GUI)
                {
                    usingBlock += Environment.NewLine + "using UnityEngine.UI;";
                }
                usingBlock += Environment.NewLine + "using Sirenix.OdinInspector;";
            }
            usingBlock += Environment.NewLine;

            return usingBlock;
        }

        static string BuildInheritance(ScriptType a_scriptType)
        {
            if (a_scriptType == ScriptType.Plugin)
            {
                return "";
            }

            string val = " : ";

            switch (a_scriptType)
            {
                case ScriptType.ScriptableObject:
                    {
                        val += "ScriptableObject";
                        break;
                    }
                case ScriptType.Editor:
                    {
                        val += "Editor";
                        break;
                    }
                case ScriptType.MonoBehaviour:
                    {
                        val += "MonoBehaviour";
                        break;
                    }
                case ScriptType.GUI:
                    {
                        val += "MonoBehaviour";
                        break;
                    }
            }
        

            return val;
        }
    }
}