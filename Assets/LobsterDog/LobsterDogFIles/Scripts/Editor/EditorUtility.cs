using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LobsterDog
{
    public static class EditorUtility
    {
        static string[] defaultFoldersToSearch = new string[] { "Assets/_Project" };

        public static T[] GetAllInstances<T>() where T : UnityEngine.Object
        {
            return GetAllInstances<T>(defaultFoldersToSearch);
        }

        public static T[] GetAllInstances<T>(string[] foldersToSearch) where T : UnityEngine.Object
        {
            string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name, foldersToSearch);  //FindAssets uses tags check documentation for more info
            T[] a = new T[guids.Length];
            for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                a[i] = AssetDatabase.LoadAssetAtPath<T>(path);
            }

            return a;
        }
    }
}