using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;

namespace LobsterDog
{
	public class GUITools : OdinEditorWindow
    {
        [MenuItem("Lobster Dog/Window/GUI Tools")]
        static void OpenWindow()
        {
            GetWindow<GUITools>().Show();
        }

        [SerializeField, TabGroup("GUI States"), TableList] List<GUIStateView> allGUIStates = new List<GUIStateView>();

        [SerializeField, TabGroup("Colours")] List<Color> colorPalette = new List<Color>();

        void OnInspectorUpdate()
        {
            allGUIStates.Clear();
            GUIState[] states = EditorUtility.GetAllInstances<GUIState>();
            for (int i = 0; i < states.Length; ++i)
            {
                allGUIStates.Add(new GUIStateView(states[i]));
            }
        }

        [Button("Add New", ButtonSizes.Small), HorizontalGroup("Actions")]
        void AddGUIState()
        {
            if (newGUIStateName != "")
            {
                GUIState state = (GUIState)ScriptableObject.CreateInstance("GUIState");
                AssetDatabase.CreateAsset(state, "Assets/_Project/Data/GUIStates/" + newGUIStateName + ".asset");
            }
        }

        [SerializeField, HorizontalGroup("Actions")] string newGUIStateName = "";
    }

    public class GUIStateView
    {
        [DisplayAsString, ShowInInspector] public string Name { get { return guiState != null ? guiState.name : ""; } }

        GUIState guiState = null;
        public GUIState GUIState { get { return guiState; } }


        [ButtonGroup("Actions")]
        [Button("Switch", ButtonSizes.Medium)]
        public void Switch()
        {
            if (EditorApplication.isPlaying)
            {
                if (GUIStateManager.Instance != null)
                {
                    GUIStateManager.Instance.SetState(guiState);
                }
            }
            else
            {
                GUIStateMember[] allMembers = GameObject.FindObjectsOfType<GUIStateMember>();
                for (int i = 0; i < allMembers.Length; ++i)
                {
                    bool on = (allMembers[i].State == guiState);

                    //Check if any of it's children match this state. If so, then it will have to be on.
                    if (!on)
                    {
                        GUIStateMember[] childMembers = allMembers[i].GetComponentsInChildren<GUIStateMember>();
                        for (int j = 0; j < childMembers.Length; ++j)
                        {
                            if (childMembers[j].State == guiState)
                            {
                                on = true;
                                break;
                            }
                        }
                    }

                    allMembers[i].GetComponent<GUIScreen>().ToggleOnOff(on, true);
                }
            }
        }

        [ButtonGroup("Actions")]
        [Button("Select", ButtonSizes.Medium)]
        void Select()
        {
            GUIStateMember[] allMembers = GameObject.FindObjectsOfType<GUIStateMember>();
            for (int i = 0; i < allMembers.Length; ++i)
            {
                if(allMembers[i].State == guiState)
                {
                    Selection.activeGameObject = allMembers[i].gameObject;


                    if (allMembers[i].gameObject.transform.childCount > 0)
                    {
                        EditorGUIUtility.PingObject(allMembers[i].gameObject.transform.GetChild(0));
                    }

                    EditorGUIUtility.PingObject(allMembers[i].gameObject);
                    break;
                }
            }
        }

        public GUIStateView(GUIState a_guiState)
        {
            guiState = a_guiState;
        }
    }
}