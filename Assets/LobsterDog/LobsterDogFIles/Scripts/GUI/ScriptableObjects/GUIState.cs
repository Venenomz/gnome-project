//Scriptable object representing a distinct state (or substate) for the UI (eg - ingame, mainmenu, options, etc). Any object with a GUIStateMember assigned to this state will be enabled when the GUIStateManager is set to this state.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [CreateAssetMenu(menuName = "Lobster Dog/GUI/GUIState")]
    public class GUIState : LobsterDogScriptable
    {
        [SerializeField, ReadOnly] List<GUIStateMember> members = new List<GUIStateMember>();

        /// <summary>
        /// Used by the GUIStateMember to register itself to a GUIState
        /// </summary>
        /// <param name="a_member">The GUIState Member to register</param>
        public void Register(GUIStateMember a_member)
        {
            if (!members.Contains(a_member))
            {
                members.Add(a_member);
            }
        }

        /// <summary>
        /// Used by the GUIStateMember to unregister itself to a GUIState
        /// </summary>
        /// <param name="a_member">The GUIState Member to unregister</param>
        public void Unregister(GUIStateMember a_member)
        {
            if (members.Contains(a_member))
            {
                members.Remove(a_member);
            }
        }

        /// <summary>
        /// Used by the GUIStateManager to activate or deactivate this GUIState (applying to all registered GUIStateMemebers)
        /// </summary>
        /// <param name="a_active">Whether the GUIState will be set to active or inactive</param>
        public void SetActive(bool a_active)
        {
            for (int i = 0; i < members.Count; ++i)
            {
                members[i].SetActive(a_active);
            }
        }
    }
}