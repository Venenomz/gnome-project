using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/DraggableGUI")]
    [DisallowMultipleComponent]
	public class DraggableGUI : LobsterDogBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        #region Reference Fields
        #endregion

        bool isDragging = false;

        Vector3 offset = Vector3.zero;

        public void OnPointerDown(PointerEventData a_data)
        {
            isDragging = true;
            offset = TransformRef.position - Input.mousePosition;
        }

        public void OnPointerUp(PointerEventData a_data)
        {
            isDragging = false;
        }

        void Update()
        {
            if (isDragging)
            {
                TransformRef.position = Input.mousePosition + offset;
            }
        }
    }
}