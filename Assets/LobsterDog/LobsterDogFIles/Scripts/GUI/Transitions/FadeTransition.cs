using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/FadeTransition")]
    [DisallowMultipleComponent]
    public class FadeTransition : GUITransition
    {
        #region Reference Fields
        CanvasGroup thisCanvasGroup = null;
        CanvasGroup ThisCanvasGroup
        {
            get
            {
                if (thisCanvasGroup == null)
                {
                    thisCanvasGroup = GetComponent<CanvasGroup>();

                    if (thisCanvasGroup == null)
                    {
                        thisCanvasGroup = gameObject.AddComponent<CanvasGroup>();
                    }
                }
                return thisCanvasGroup;
            }
        }
        #endregion

        public override void SetTransitionValue(float01 a_value)
        {
            ThisCanvasGroup.alpha = a_value.Value;
        }
    }
}