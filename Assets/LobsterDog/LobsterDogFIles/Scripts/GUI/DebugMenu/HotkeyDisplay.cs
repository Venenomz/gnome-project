using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/GUI/DebugMenu/HotkeyDisplay")]
	[DisallowMultipleComponent]
	public class HotkeyDisplay : GUIContentLoader
	{
        #region Reference Fields
        [SerializeField, FoldoutGroup("References")] KeyboardActions hotkeys = null;
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods
        protected override void Awake()
        {
            base.Awake();

            string[] hotkeysText = hotkeys.GetKeyActionsAsText();

            for(int i = 0; i < hotkeysText.Length; ++i)
            {
                (SpawnContent(hotkeysText[i]) as HotkeyText).Initialise(string.Format("{0} + {1}", hotkeys.MasterKeyName, hotkeysText[i]));
            }
        }
        #endregion
    }
}