using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System.Linq;

namespace LobsterDog
{
	[DisallowMultipleComponent]
	public static class CustomUIMethods
	{
        public static Toggle GetActive(this ToggleGroup a_group)
        {
            return a_group.ActiveToggles().FirstOrDefault();
        }
    }
}