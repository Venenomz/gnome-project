using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/GUI/ApplicationInput")]
	[DisallowMultipleComponent]
	public class ApplicationInput : MonoBehaviour
	{
        #region Reference Fields
        [SerializeField, FoldoutGroup("References")] GUIScreen _debugConsole = null;
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods
        public virtual void OnToggleDebugConsole()
        {
            _debugConsole.Toggle();
        }

        public virtual void OnQuit()
        {
            Application.Quit();
        }

        public virtual void OnReset()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
        #endregion
    }
}