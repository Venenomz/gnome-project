using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/GUI/GUILoadedContent")]
    [DisallowMultipleComponent] [RequireComponent(typeof(CanvasGroup))]
    public class GUILoadedContent : LobsterDogBehaviour
    {
        protected CanvasGroup thisCanvasGroup;
        public CanvasGroup CanvasGroup { get { return thisCanvasGroup; } }
        public RectTransform Transform { get { return RectTransformRef; } }

        protected virtual void Awake()
        {
            thisCanvasGroup = GetComponent<CanvasGroup>();
        }

        public virtual void SetAlpha(float01 a_newAlpha)
        {
            thisCanvasGroup.alpha = a_newAlpha.Value;
        }

        public virtual void Remove()
        {
            Destroy(gameObject);
        }
	}
}