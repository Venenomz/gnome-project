using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/GUI/SceneTimeout")]
	[DisallowMultipleComponent]
	public class SceneTimeout : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, BoxGroup("References")]
		#endregion

		[SerializeField] float timeOutTime = 0;

		float timer = 0;

		void Awake()
		{
			timer = timeOutTime;
		}

		void Update()
		{
			if(Input.GetMouseButton(0) || Input.touchCount > 0)
			{
				timer = timeOutTime;
			}
			else
			{
				timer -= Time.deltaTime;

				if(timer < 0)
				{
					SceneManager.LoadScene(0);
				}
			}
		}
	}
}