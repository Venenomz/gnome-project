using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Debug/FPSDisplay")]
    [DisallowMultipleComponent]
	public class FPSDisplay : MonoBehaviour
	{
        #region Reference Fields
        [SerializeField] Text fpsDisplay = null;
        #endregion

        [SerializeField] float updateInterval = 0.5f;

        float accum = 0; // FPS accumulated over the interval
        int frames = 0; // Frames drawn over the interval
        float timeleft; // Left time for current interval

        void Update()
        {
            if (fpsDisplay != null)
            {
                timeleft -= Time.deltaTime;
                accum += Time.timeScale / Time.deltaTime;
                ++frames;

                if (timeleft <= 0.0)
                {
                    float fps = accum / frames;
                    fpsDisplay.text = "FPS: " + System.String.Format("{0:F2}", fps);

                    timeleft = updateInterval;
                    accum = 0.0F;
                    frames = 0;
                }
            }
        }
    }
}