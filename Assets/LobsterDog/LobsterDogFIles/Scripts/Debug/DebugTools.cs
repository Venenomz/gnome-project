using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Diagnostics;
using System;

namespace LobsterDog
{
    [DisallowMultipleComponent]
    public static class DebugTools
	{
        #region Reference Fields
        #endregion

        static string internalLog = "";
        public static string InternalLog { get { return internalLog; } private set { internalLog = value; } }

        public enum LogMessageColour
        {
            Purple,
            White,
            Black,
            Green,
            Orange,
            Blue
        }

        static LogMessageColour defaultTagColour = LogMessageColour.Green;

        static Dictionary<string, Stopwatch> activeStopwatches = new Dictionary<string, Stopwatch>();

        public static void SpawnDebugMarker(Vector3 a_position, Color a_colour, float a_scale = 1)
        {
            GameObject marker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            marker.transform.position = a_position;
            marker.GetComponent<Renderer>().material.color = a_colour;
            marker.transform.localScale = new Vector3(a_scale, a_scale, a_scale);
            MonoBehaviour.Destroy(marker.GetComponent<Collider>());
        }

        public static void StartTimer(string a_name)
        {
            if (!activeStopwatches.ContainsKey(a_name))
            {
                Stopwatch newTimer = new Stopwatch();
                activeStopwatches.Add(a_name, newTimer);
                newTimer.Start();
            }
        }

        public static double StopTimer(string a_name, bool a_logResult = true)
        {
            if (activeStopwatches.ContainsKey(a_name))
            {
                activeStopwatches[a_name].Stop();

                if (a_logResult)
                {
                    Log("Result for " + a_name + ": " + activeStopwatches[a_name].Elapsed, "Debug Timer", LogMessageColour.White);
                }

                activeStopwatches.Remove(a_name);

                return activeStopwatches[a_name].Elapsed.TotalSeconds;
            }

            return 0;
        }

        public static void DrawRay(Ray a_ray, float a_distance, Color a_color)
        {
            UnityEngine.Debug.DrawRay(a_ray.origin, a_ray.origin + (a_ray.direction * a_distance), a_color);
        }

        public static void Log(string a_message)
        {
            InternalLog += a_message + Environment.NewLine;
            UnityEngine.Debug.Log(a_message);
        }

        public static void Log(string a_message, string a_tag)
        {
            Log(LogColourToString("<b>[" + a_tag + "]</b> ", defaultTagColour) + a_message);
        }

        public static void Log(string a_message, LogMessageColour a_colour)
        {
            Log(LogColourToString(a_message, a_colour));
        }

        public static void Log(string a_message, string a_tag, LogMessageColour a_colour)
        {
            Log(LogColourToString(a_message, a_colour), a_tag);
        }

        static string LogColourToString(string a_message, LogMessageColour a_colour)
        {
            return "<color=" + a_colour.ToString() + ">" + a_message + "</color>";
        }
    }
}