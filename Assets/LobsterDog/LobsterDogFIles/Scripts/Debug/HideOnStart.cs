using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Debug/HideOnStart")]
    [DisallowMultipleComponent]
    public class HideOnStart : MonoBehaviour
    {
		#region Reference Fields
        #endregion

        public enum HideType
        {
            Destroy,
            Disable,
            HideRenderer
        }

        [SerializeField] [BoxGroup("Hide on")] bool editor = true;
        [SerializeField] [BoxGroup("Hide on")] bool android = true;
        [SerializeField] [BoxGroup("Hide on")] bool iOS = true;
        [SerializeField] [BoxGroup("Hide on")] bool windows = true;
        [SerializeField] [BoxGroup("Hide on")] bool mac = true;
        [SerializeField] [BoxGroup("Hide on")] bool webGL = true;

        [SerializeField] HideType type = HideType.Destroy;

        void Awake()
        {
            if (((Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor) && editor)
                || (Application.platform == RuntimePlatform.Android && android)
                || (Application.platform == RuntimePlatform.IPhonePlayer && iOS)
                || (Application.platform == RuntimePlatform.WebGLPlayer && webGL)
                || (Application.platform == RuntimePlatform.WindowsPlayer && windows)
                || (Application.platform == RuntimePlatform.OSXPlayer && mac))
            {
                Hide();
            }
        }

        void Hide()
        {
            switch (type)
            {
                case HideType.Destroy:
                    {
                        Destroy();
                        break;
                    }
                case HideType.Disable:
                    {
                        Disable();
                        break;
                    }
                case HideType.HideRenderer:
                    {
                        HideRenderer();
                        break;
                    }
                default:
                    {
                        DebugTools.Log("Unhandled HideOnStart type encountered. Default is destroy.", "Warning");
                        Destroy();
                        break;
                    }
            }
        }

        void Destroy()
        {
            Destroy(gameObject);
        }

        void Disable()
        {
            gameObject.SetActive(false);
        }

        void HideRenderer()
        {
            Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderers.Length; ++i)
            {
                renderers[i].enabled = false;
            }
        }
    }
}