using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/SimpleRotate")]
    [DisallowMultipleComponent]
	public class SimpleRotate : LobsterDogBehaviour
	{
        #region Reference Fields
        #endregion

        [SerializeField] Vector3 rotationSpeed = Vector3.zero;
        public Vector3 RotationSpeed { get { return rotationSpeed; } set { rotationSpeed = value; } }

        void Update()
        {
            Rotate();
        }

        protected virtual void Rotate()
        {
            TransformRef.Rotate(rotationSpeed * Time.deltaTime);
        }
    }
}