using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Anims/TransformAnim")]
    [DisallowMultipleComponent]
	public class TransformAnim : AnimEffect
    {
        #region Reference Fields
        #endregion

        [SerializeField] bool local = true;

        [Header("Initial")]
        [SerializeField] Vector3 initialPos = Vector3.zero;
        [SerializeField] Vector3 intialRot = Vector3.zero;
        [SerializeField] Vector3 initialScale = Vector3.zero;

        [Header("Final")]
        [SerializeField] Vector3 finalPos = Vector3.zero;
        [SerializeField] Vector3 finalRot = Vector3.zero;
        [SerializeField] Vector3 finalScale = Vector3.zero;

        [Button("Store Initial")]
        void StoreInitial()
        {
            initialPos = local ? TransformRef.localPosition : TransformRef.position;
            intialRot = local ? TransformRef.localEulerAngles : TransformRef.eulerAngles;
            initialScale = TransformRef.localScale;
        }

        [Button("Store Final")]
        void StoreFinal()
        {
            finalPos = local ? TransformRef.localPosition : TransformRef.position;
            finalRot = local ? TransformRef.localEulerAngles : TransformRef.eulerAngles;
            finalScale = TransformRef.localScale;
        }

        [Button("Set to Initial")]
        void SetToInitial()
        {
            if (local)
            {
                TransformRef.localPosition = initialPos;
                TransformRef.localEulerAngles = intialRot;
            }
            else
            {
                TransformRef.position = initialPos;
                TransformRef.eulerAngles = intialRot;
            }

            TransformRef.localScale = initialScale;
        }

        [Button("Set to Final")]
        void SetToFinal()
        {
            if (local)
            {
                TransformRef.localPosition = finalPos;
                TransformRef.localEulerAngles = finalRot;
            }
            else
            {
                TransformRef.position = finalPos;
                TransformRef.eulerAngles = finalRot;
            }

            TransformRef.localScale = finalScale;
        }

        protected override void OnValueChanged(float a_newValue)
        {
            if (local)
            {
                TransformRef.localPosition = Vector3.LerpUnclamped(initialPos, finalPos, a_newValue);
                TransformRef.localEulerAngles = Vector3.LerpUnclamped(intialRot, finalRot, a_newValue);
            }
            else
            {
                TransformRef.position = Vector3.LerpUnclamped(initialPos, finalPos, a_newValue);
                TransformRef.eulerAngles = Vector3.LerpUnclamped(intialRot, finalRot, a_newValue);
            }

            TransformRef.localScale = Vector3.LerpUnclamped(initialScale, finalScale, a_newValue);
        }
    }
}