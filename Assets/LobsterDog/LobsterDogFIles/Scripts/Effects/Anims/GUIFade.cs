using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Anims/GUIFade")]
    [DisallowMultipleComponent][RequireComponent(typeof(CanvasGroup))]
	public class GUIFade : AnimEffect
	{
        #region Reference Fields
        [SerializeField, FoldoutGroup("References")] CanvasGroup canvasGroup = null;
        #endregion

        protected override void Awake()
        {
            base.Awake();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        protected override void OnValueChanged(float a_newValue)
        {
            canvasGroup.alpha = a_newValue;
        }
    }
}