using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Anims/ScaleAnim")]
    [DisallowMultipleComponent]
	public class ScaleAnim : Vec3Anim
    {
        #region Reference Fields
        #endregion
            
        [Button("Store Initial")]
        void StoreInitial()
        {
            initialValue = TransformRef.localScale;
        }

        [Button("Store Final")]
        void StoreFinal()
        {
            finalValue = TransformRef.localScale;
        }

        [Button("Set to Initial")]
        void SetToInitial()
        {
            TransformRef.localScale = initialValue;
        }

        [Button("Set to Final")]
        void SetToFinal()
        {
            TransformRef.localScale = finalValue;
        }

        protected override void Set(Vector3 a_newValue)
        {
            TransformRef.localScale = a_newValue;
        }
    }
}