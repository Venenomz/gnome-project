using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Anims/AnimEngine")]
    [DisallowMultipleComponent]
	public class AnimEngine : MonoBehaviour
    {
        public float Value { get; private set; }

        [SerializeField] AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1));        
        [SerializeField] bool playOnAwake = true;

        List<AnimEffect> subscribedEffects = new List<AnimEffect>();



        [SerializeField] float timeSeconds = 1.0f;
        public float TimeSeconds { get { return timeSeconds; } }
        float elapsed = 0;
        int timeDirection = 1;

        public enum WrapMode { Clamp, Loop, PingPong };
        [SerializeField] WrapMode wrapMode = WrapMode.Clamp;

        [SerializeField, HideIf("isClampMode")] bool limitIterations = false;
        [Tooltip("One iteration is going from 1 to 0 or 0 to 1 once.")]
        [SerializeField, ShowIf("limitIterations")] int maxIterations = 1;
        [ShowIf("limitIterations")] int currentIterations = 0;

        bool playing = false;

        [SerializeField, ShowIf("isClampMode")] UnityEvent onComplete = null;

#if UNITY_EDITOR
#pragma warning disable 0414
        [SerializeField, ProgressBar("Value", 1)] float displayedValue = 0;
        [SerializeField, ProgressBar("Iterations", 1), ShowIf("limitIterations")] float iterationsValue = 0;

        bool isClampMode () { return wrapMode == WrapMode.Clamp; }
#pragma warning restore 0414
#endif

        void Awake()
        {
            Value = 0;
            if (playOnAwake)
            {
                Restart();
            }
        }

        void Start()
        {
            UpdateEffects();
        }

        public void Subscribe(AnimEffect a_subscriber)
        {
            subscribedEffects.Add(a_subscriber);
        }

        public void Unsubscribe(AnimEffect a_unsubscriber)
        {
            subscribedEffects.Remove(a_unsubscriber);
        }

        [Button()]
        public void Restart()
        {
            Reset(true);
            Play();
        }

        [Button()]
        public void Reset()
        {
            Reset(true);
        }

        public void Reset(bool a_clearIterations)
        {
            elapsed = 0;
            timeDirection = 1;

            if (a_clearIterations)
            {
                currentIterations = 0;
            }
        }

        [Button()]
        public void Play()
        {
            playing = true;
        }

        [Button()]
        public void Stop()
        {
            playing = false;
        }

        void Update()
        {
            EvaluateValue();
        }

        void EvaluateValue()
        {
            if (playing)
            {
                elapsed += Time.deltaTime * timeDirection;
                Value = curve.Evaluate(elapsed / timeSeconds);
                UpdateEffects();

                if (elapsed <= 0 || elapsed >= timeSeconds)
                {
                    ++currentIterations;

                    if (limitIterations && currentIterations >= maxIterations)
                    {
                        playing = false;
                    }

                    switch (wrapMode)
                    {
                        case WrapMode.Clamp:
                        {
                            elapsed = Mathf.Clamp(elapsed, 0, timeSeconds);
                            playing = false;
                            onComplete.Invoke();
                            break;
                        }
                        case WrapMode.Loop:
                        {
                            Reset(false);
                            break;
                        }
                        case WrapMode.PingPong:
                        {
                            if (elapsed < 0)
                            {
                                elapsed *= -1; //if it's negative, start the ping pong at that value
                            }
                            else if (elapsed > timeSeconds)
                            {
                                elapsed = timeSeconds - (elapsed - timeSeconds);
                            }

                            timeDirection *= -1;
                            break;
                        }
                    }
                }

#if UNITY_EDITOR
                displayedValue = Value;
                iterationsValue = (float)currentIterations / (float)maxIterations;
#endif
            }
        }

        void UpdateEffects()
        {
            for (int i = 0; i < subscribedEffects.Count; ++i)
            {
                subscribedEffects[i].ValueChanged();
            }
        }
	}
}