using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/Effects/SimpleFollow")]
	[DisallowMultipleComponent]
	public class SimpleFollow : Targeter
	{
		#region Reference Fields
        #endregion

        [SerializeField] protected FloatRef speed = null;

        protected virtual void Update()
        {
            Follow();
        }

        protected virtual void Follow()
        {
            TransformRef.position = Vector3.MoveTowards(TransformRef.position, Target.position + offset, Time.deltaTime * speed);
        }
	}
}