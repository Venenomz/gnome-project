using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Effects/Targeter")]
    public class Targeter : LobsterDogBehaviour
    {
        [SerializeField, BoxGroup("Targeter")] protected bool shareTarget = false;
        [SerializeField, BoxGroup("Targeter"), HideIf("shareTarget")] protected Transform target = null;
        [SerializeField, BoxGroup("Targeter"), ShowIf("shareTarget")] protected Targeter sharedTarget = null;
        [SerializeField, BoxGroup("Targeter")] protected Vector3 offset = Vector3.zero;
        protected Transform originalTarget = null;

        public Transform Target { get { return shareTarget ? sharedTarget.Target : target; } }

        protected virtual void Awake()
        {
            if (!shareTarget)
            {
                originalTarget = target;
            }
        }

        public virtual void ChangeTarget(Transform a_newTaget)
        {
            if (!shareTarget)
            {
                target = a_newTaget;
            }
        }

        public virtual void SetToOriginalTarget()
        {
            if (!shareTarget)
            {
                target = originalTarget;
            }
        }
    }
}