using UnityEngine;
using UnityEngine.UI;


namespace LobsterDog
{
    public static class ExtensionsUI
    {
        #region Methods

        ////////////////////////////////////////Scroll Rect////////////////////////////////////////////////////

        public static void ScrollToTop(this ScrollRect rect)
        {
            rect.normalizedPosition = new Vector2(0, 1);
        }

        public static void ScrollToBottom(this ScrollRect rect)
        {
            rect.normalizedPosition = new Vector2(0, 0);
        }

        public static void StopScrolling(this ScrollRect rect)
        {
            rect.velocity = Vector2.zero;
        }

        ////////////////////////////////////////Canvas////////////////////////////////////////////////////

        public static Canvas GetParentCanvas(this RectTransform rectT)
        {
            RectTransform parent = rectT;

            Canvas parentCanva = rectT.GetComponent<Canvas>();

            int searchIndex = 0;

            while (parentCanva == null || searchIndex > 50)
            {
                parentCanva = rectT.GetComponentInParent<Canvas>();

                if (parentCanva == null)
                {
                    parent = parent.parent.GetComponent<RectTransform>();
                    searchIndex++;
                }
            }

            return parentCanva;
        }

        public static Vector2 TransformInputBasedOnCanvasType(this Vector2 input, Canvas canvas)
        {
            if (canvas.renderMode == RenderMode.ScreenSpaceOverlay)
                return canvas.GetEventCamera().ScreenToWorldPoint(input);

            return input;
        }

        public static Vector3 TransformInputBasedOnCanvasType(this Vector2 input, RectTransform rt)
        {
            var canvas = rt.GetParentCanvas();
            if (input == Vector2.zero || canvas.renderMode == RenderMode.ScreenSpaceOverlay)
            {
                return input;
            }
            else
            {
                RectTransformUtility.ScreenPointToLocalPointInRectangle(rt, input, canvas.GetEventCamera(), out Vector2 movePos);

                return canvas.transform.TransformPoint(movePos);
            }
        }

        ////////////////////////////////////////Canemra////////////////////////////////////////////////////

        public static Camera GetEventCamera(this Canvas input)
        {
            return input.worldCamera == null ? Camera.main : input.worldCamera;
        }

        ////////////////////////////////////////Rect Transform/////////////////////////////////////////////

        public static Vector2 SwitchRectToTransform(this RectTransform from, RectTransform to)
        {
            Vector2 localPoint;

            Vector2 fromPivotDerivedOffset = new Vector2(from.rect.width * from.pivot.x + from.rect.xMin, from.rect.height * from.pivot.y + from.rect.yMin);

            Vector2 screenP = RectTransformUtility.WorldToScreenPoint(null, from.position);

            screenP += fromPivotDerivedOffset;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(to, screenP, null, out localPoint);

            Vector2 pivotDerivedOffset = new Vector2(to.rect.width * to.pivot.x + to.rect.xMin, to.rect.height * to.pivot.y + to.rect.yMin);

            return to.anchoredPosition + localPoint - pivotDerivedOffset;
        }

        #endregion
    }
}