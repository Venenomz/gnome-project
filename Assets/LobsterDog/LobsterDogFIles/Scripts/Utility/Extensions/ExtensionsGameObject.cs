﻿using UnityEngine;
using System;
using System.Collections;

namespace LobsterDog
{
    public static class ExtensionsGameObject
    {
        /// <summary>
        /// Does GetComponent but logs an error if is not found
        /// </summary>
        /// <returns>The component safe.</returns>
        public static T GetComponentSafe<T>(this GameObject obj) where T : Component
        {
            T component = obj.GetComponent<T>();

            if (component == null)
            {
                if (Utils.UnityEditor())
                    Debug.LogError("Expected to find component of type " + typeof(T) + " but found none", obj);
            }

            return component;
        }

        /// <summary>
        /// Returns component T if not null, otherwise adds the componenet then returns it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T GetOrAddComponent<T>(this GameObject obj) where T : Component
        {
            T res = obj.GetComponent<T>();

            if (res == null)
                res = obj.AddComponent<T>();

            return res;
        }

        /// <summary>
        /// Check if an object is a prefab or not.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsPrefab(this GameObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            return !obj.scene.IsValid() && !obj.scene.isLoaded && obj.GetInstanceID() >= 0 && !obj.hideFlags.HasFlag(HideFlags.HideInHierarchy);
        }

        /// <summary>
        /// Does GetComponentInChildren but logs an error if is not found
        /// </summary>
        /// <returns>The component safe.</returns>
        public static T GetComponentInChildrenSafe<T>(this GameObject obj) where T : Component
        {
            T component = obj.GetComponentInChildren<T>();

            if (component == null)
            {
                if (Utils.UnityEditor())
                    Debug.LogError("Expected to find component of type " + typeof(T) + " but found none", obj);
            }

            return component;
        }

        public static int GetComponentsInChildrenNonAlloc<T>(this GameObject obj, T[] foundComponents) where T : Component
        {
            int found = 0;
            for (int i = 0; i < obj.transform.childCount && found < foundComponents.Length; i++)
            {
                T comp = obj.transform.GetChild(i).gameObject.GetComponent<T>();

                if (comp != null)
                    foundComponents[found++] = comp;
            }

            return found;
        }

        //This is a hack to get shaders to work on pc from a device targeted bundle
        public static void ReapplyShaders(this GameObject go)
        {
            var renderers = go.GetComponentsInChildren<Renderer>();
            foreach (var rend in renderers)
            {
                var materials = rend.sharedMaterials;
                var shaders = new string[materials.Length];

                for (int i = 0; i < materials.Length; i++)
                    shaders[i] = (materials[i].NotNull() && materials[i].shader != null) ? materials[i].shader.name : null;

                for (int i = 0; i < materials.Length; i++)
                {
                    if (materials[i] != null && shaders[i] != null)
                        materials[i].shader = Shader.Find(shaders[i]);
                }
            }
        }
    }
}
