﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LobsterDog
{
	public static class ExtensionsTransform
	{
		//Searching
		public static Transform FindChildRecursive(this Transform me, string child)
		{

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);
				if (childTrans.name == child)
				{
					return childTrans;
				}
				else
				{
					Transform childTransRec = childTrans.FindChildRecursive(child);
					if (childTransRec != null)
					{
						return childTransRec;
					}
				}
			}


			return null;
		}

		public static void FindChildrenRecursive(this Transform me, string name, List<Transform> results)
		{

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);

				if (childTrans.name.CompareTo(name) == 0)
				{
					results.Add(childTrans);
				}

				childTrans.FindChildrenRecursive(name, results);

			}


		}

		/// <summary>
		/// Does the same thing as GetComponentInChildren, but actually works on non instantiated objects!
		/// </summary>
		public static T GetComponentInChildrenPrefab<T>(this Transform me)
		{

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);
				T comp = childTrans.GetComponent<T>();
				if (comp != null)
				{
					return comp;
				}
				else
				{
					T compRecurse = childTrans.GetComponentInChildrenPrefab<T>();
					if (compRecurse != null)
					{
						return compRecurse;
					}
				}
			}

			return default(T);
		}

		public static void SetMaterialOnAllSpriteRenderers(this Transform me, Material material)
		{
			SpriteRenderer spr = me.GetComponent<SpriteRenderer>();
			if (spr != null)
			{
				spr.sharedMaterial = material;
			}

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);
				childTrans.SetMaterialOnAllSpriteRenderers(material);
			}

		}

		public static void SetSortingLayerOnAllSpriteRenderers(this Transform me, string sortLayer)
		{
			SpriteRenderer spr = me.GetComponent<SpriteRenderer>();
			if (spr != null)
			{
				spr.sortingLayerName = sortLayer;
			}

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);
				childTrans.SetSortingLayerOnAllSpriteRenderers(sortLayer);
			}

		}

		public static void SetSortingLayerOnAllRenderers(this Transform me, string sortLayer)
		{
			Renderer spr = me.GetComponent<Renderer>();
			if (spr != null)
			{
				spr.sortingLayerName = sortLayer;
			}

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);
				childTrans.SetSortingLayerOnAllRenderers(sortLayer);
			}

		}



		public static void SetLayerOnAllChildren(this Transform me, int layer)
		{
			me.gameObject.layer = layer;

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);
				childTrans.SetLayerOnAllChildren(layer);
			}

		}


		public static void GetComponentsInChildrenPrefab<T>(this Transform me, List<T> results)
		{

			for (int i = 0; i < me.childCount; i++)
			{
				Transform childTrans = me.GetChild(i);
				T comp = childTrans.GetComponent<T>();

				if (comp != null)
				{
					results.Add(comp);
				}


				childTrans.GetComponentsInChildrenPrefab<T>(results);
			}

		}

		public static void CopyTransform(this Transform me, Transform source)
		{
			me.localPosition = source.localPosition;
			me.localRotation = source.localRotation;
			me.localScale = source.localScale;
		}

		/////////////////////////////////////////////////////////
		//Pos

		public static void SetAbsolutePos2D(this Transform me, Vector2 pos)
		{
			me.position = new Vector3(pos.x, pos.y, me.position.z);
		}

		public static void SetAbsolutePosX(this Transform me, float x)
		{
			me.position = new Vector3(x, me.position.y, me.position.z);
		}
		public static void SetAbsolutePosY(this Transform me, float y)
		{
			me.position = new Vector3(me.position.x, y, me.position.z);
		}

		public static void SetAbsolutePosZ(this Transform me, float z)
		{
			me.position = new Vector3(me.position.x, me.position.y, z);
		}

		public static void SetLocalPos2D(this Transform me, Vector2 pos)
		{
			me.localPosition = new Vector3(pos.x, pos.y, me.localPosition.z);
		}

		public static void SetLocalPos2D(this Transform me, float x, float y)
		{
			me.localPosition = new Vector3(x, y, me.localPosition.z);
		}

		public static void SetLocalPosX(this Transform me, float x)
		{
			me.localPosition = new Vector3(x, me.localPosition.y, me.localPosition.z);
		}
		public static void SetLocalPosY(this Transform me, float y)
		{
			me.localPosition = new Vector3(me.localPosition.x, y, me.localPosition.z);
		}

		public static void SetLocalPosZ(this Transform me, float z)
		{
			me.localPosition = new Vector3(me.localPosition.x, me.localPosition.y, z);
		}


		//////////////////////////////////////////////////
		// Scale
		public static void SetLocalScaleX(this Transform me, float scaleX)
		{
			me.localScale = new Vector3(scaleX, me.localScale.y, me.localScale.z);
		}
		public static void SetLocalScaleY(this Transform me, float scaleY)
		{
			me.localScale = new Vector3(me.localScale.x, scaleY, me.localScale.z);
		}

		public static void SetLocalScaleZ(this Transform me, float scaleZ)
		{
			me.localScale = new Vector3(me.localScale.x, me.localScale.y, scaleZ);
		}

		public static void SetLocalScale2D(this Transform me, Vector2 scale)
		{
			me.localScale = new Vector3(scale.x, scale.y, me.localScale.z);
		}

		public static void SetLocalScale(this Transform me, float allscale)
		{
			me.localScale = new Vector3(allscale, allscale, allscale);
		}


		//////////////////////////////////////////////////
		// rot


		public static void SetLocalRotX(this Transform me, float rotX)
		{
			Vector3 euler = me.localRotation.eulerAngles;
			me.localRotation = Quaternion.Euler(rotX, euler.y, euler.z);
		}

		public static void SetLocalRotY(this Transform me, float rotY)
		{
			Vector3 euler = me.localRotation.eulerAngles;
			me.localRotation = Quaternion.Euler(euler.x, rotY, euler.z);
		}

		public static void SetLocalRotZ(this Transform me, float rotZ)
		{
			Vector3 euler = me.localRotation.eulerAngles;
			me.localRotation = Quaternion.Euler(euler.x, euler.y, rotZ);
		}

		public static void SetAbsoluteRotX(this Transform me, float rotX)
		{
			Vector3 euler = me.rotation.eulerAngles;
			me.rotation = Quaternion.Euler(rotX, euler.y, euler.z);
		}

		public static void SetAbsoluteRotY(this Transform me, float rotY)
		{
			Vector3 euler = me.rotation.eulerAngles;
			me.rotation = Quaternion.Euler(euler.x, rotY, euler.z);
		}

		public static void SetAbsoluteRotZ(this Transform me, float rotZ)
		{
			Vector3 euler = me.rotation.eulerAngles;
			me.rotation = Quaternion.Euler(euler.x, euler.y, rotZ);
		}

		public static Vector2 Rotate(this Vector2 v, float degrees)
		{
			float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
			float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

			float tx = v.x;
			float ty = v.y;
			v.x = (cos * tx) - (sin * ty);
			v.y = (sin * tx) + (cos * ty);
			return v;
		}

	}
}
