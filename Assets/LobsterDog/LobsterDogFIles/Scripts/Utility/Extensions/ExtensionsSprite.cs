﻿using UnityEngine;
using System.Collections;

namespace LobsterDog
{
	public static class ExtensionsSprite
	{

		//Extensions to work with the special sprite shader
		public static void SetSpecialAlpha(this SpriteRenderer sprite, float a)
		{
			Color col = sprite.color;
			col.a = a;
			sprite.color = col;
		}

		public static void SetAlpha(this SpriteRenderer sprite, float a)
		{
			Color col = sprite.color;
			col.a = a;
			sprite.color = col;
		}

		public static void SetSpecialBrightness(this SpriteRenderer sprite, float brightness)
		{
			Color col = sprite.color;
			col.b = brightness;
			sprite.color = col;
		}

		public static void SetSpecialRamp(this SpriteRenderer sprite, float ramp)
		{
			Color col = sprite.color;
			col.r = ramp;
			sprite.color = col;
		}

		public static void ApplyTemplateSprite(this SpriteRenderer target, SpriteRenderer source)
		{
			if (source != null)
			{
				Transform sourceTrans = source.transform;
				Transform targetTrans = target.transform;

				targetTrans.localScale = sourceTrans.localScale;
				targetTrans.localPosition = sourceTrans.localPosition;
				targetTrans.localRotation = sourceTrans.localRotation;

				target.flipX = source.flipX;
				target.flipY = source.flipY;
				target.sprite = source.sprite;
				target.sortingLayerID = source.sortingLayerID;
				target.sortingOrder = source.sortingOrder;
				target.material = source.sharedMaterial;
				target.color = source.color;

				target.enabled = true;
			}
			else
			{
				target.enabled = false;
			}

		}
	}
}
