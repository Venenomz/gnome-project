﻿using UnityEngine;
using System.Collections;
using System.Text;

namespace LobsterDog
{
	public static class ExtensionsStringBuilder
	{
		const char CHAR_SPACE = ' ';
		const char CHAR_COLON = ':';
		const char CHAR_ZERO = '0';
		const char CHAR_POINT = '.';

		public static bool NotNullOrEmpty(this string me)
		{
			return string.IsNullOrEmpty(me) == false;
		}

		public static bool IsNullOrEmpty(this string me)
		{
			return string.IsNullOrEmpty(me);
		}

		public static bool ComparePrefix(this string me, string prefix)
		{
			for (int i = 0; i < prefix.Length; i++)
			{
				if (i >= me.Length || me[i] != prefix[i])
				{
					return false;
				}
			}

			return true;
		}

		public static bool ComparePostPrefix(this string me, string postfix)
		{
			for (int i = 0; i < postfix.Length; i++)
			{
				int stringIndex = i + (me.Length - postfix.Length);

				if (stringIndex < 0 || stringIndex >= me.Length || me[stringIndex] != postfix[i])
				{
					return false;
				}
			}

			return true;
		}

		public static void Clear(this StringBuilder me)
		{
			me.Length = 0;
		}

		public static void AppendPositiveInt(this StringBuilder me, int num)
		{
			if (num > 0)
			{
				AppendIntRecursive(me, num);
			}
			else
			{
				me.Append(CHAR_ZERO);
			}
		}

		static void AppendIntRecursive(StringBuilder str, int num)
		{

			//got to last
			if (num == 0)
			{
				return;
			}
			else
			{
				int numDown = num / 10;
				AppendIntRecursive(str, numDown);
				str.Append((char)(CHAR_ZERO + (num - (numDown * 10))));
			}
		}

		public enum TimerComponent
		{
			SPLITS,
			SECONDS,
			MINS,
			HOURS,
			DAYS
		}

		public static string Reverse(this string text)
		{
			char[] cArray = text.ToCharArray();
			string reverse = string.Empty;
			for (int i = cArray.Length - 1; i > -1; i--)
			{
				reverse += cArray[i];
			}
			return reverse;
		}


		public static void FormatTimer(this StringBuilder str, System.TimeSpan span, TimerComponent minComponent, TimerComponent maxComponent)
		{

			str.Clear();

			int days = span.Days;
			int mins = span.Minutes;
			int hours = span.Hours;
			int seconds = span.Seconds;
			int hundredths = (int)(span.Milliseconds / 10);

			bool showDays = (TimerComponent.DAYS <= maxComponent && TimerComponent.DAYS >= minComponent);
			bool showHours = (TimerComponent.HOURS <= maxComponent && TimerComponent.HOURS >= minComponent);
			bool showMins = (TimerComponent.MINS <= maxComponent && TimerComponent.MINS >= minComponent);
			bool showSeconds = (TimerComponent.SECONDS <= maxComponent && TimerComponent.SECONDS >= minComponent);
			bool showSplits = (TimerComponent.SPLITS <= maxComponent && TimerComponent.SPLITS >= minComponent);

			if (showDays)
			{


				str.AppendPositiveInt(days);

				if (showHours)
				{
					str.Append(CHAR_COLON);
				}


			}

			if (showHours)
			{

				if (hours < 10)
				{
					str.Append(CHAR_ZERO);
				}
				str.AppendPositiveInt(hours);

				if (showMins)
				{


					str.Append(CHAR_COLON);
				}


			}


			if (showMins)
			{

				if (mins < 10)
				{
					str.Append(CHAR_ZERO);
				}
				str.AppendPositiveInt(mins);
				if (showSeconds)
				{


					str.Append(CHAR_COLON);
				}


			}

			if (showSeconds)
			{
				if (seconds < 10)
				{
					str.Append(CHAR_ZERO);
				}

				str.AppendPositiveInt(seconds);


				if (showSplits)
				{
					str.Append('.');
				}

			}


			if ((TimerComponent.SPLITS <= maxComponent && TimerComponent.SPLITS >= minComponent))
			{

				if (hundredths < 10)
				{
					str.Append(CHAR_ZERO);
				}

				str.AppendPositiveInt(hundredths);
			}

		}

		public static void FormatTimer(this StringBuilder str, System.TimeSpan span, TimerComponent minComponent, int numberOfComponents, string dayTag, string hourTag, string minTag, string secTag, bool appendHourMinuteTags = true)
		{

			str.Clear();

			int days = span.Days;
			int hours = span.Hours;
			int mins = span.Minutes;
			int seconds = span.Seconds;

			int numComponentsUsed = 0;

			bool hasStartedBuilding = false;

			if (TimerComponent.DAYS >= minComponent && (days > 0))
			//if (days > 0)
			{
				str.AppendPositiveInt(days);

				if (appendHourMinuteTags)
				{
					str.Append(dayTag);
					str.Append(CHAR_SPACE);
				}
				else
				{
					str.Append(CHAR_COLON);
				}
				numComponentsUsed++;
				hasStartedBuilding = true;

				if (numComponentsUsed >= numberOfComponents)
				{
					return;
				}
			}

			if (TimerComponent.HOURS >= minComponent && (hours > 0 || days > 0))
			{

				str.AppendPositiveInt(hours);

				if (appendHourMinuteTags)
				{
					str.Append(hourTag);
					str.Append(CHAR_SPACE);
				}
				else
				{
					str.Append(CHAR_COLON);
				}


				numComponentsUsed++;

				if (numComponentsUsed >= numberOfComponents)
				{
					return;
				}

			}

			if (TimerComponent.MINS >= minComponent && (mins > 0 || hours > 0))
			{
				str.AppendPositiveInt(mins);

				if (appendHourMinuteTags)
				{
					str.Append(minTag);
					str.Append(CHAR_SPACE);
				}
				else
				{
					str.Append(CHAR_COLON);
				}

				numComponentsUsed++;

				if (numComponentsUsed >= numberOfComponents)
				{
					return;
				}

			}

			if (TimerComponent.SECONDS >= minComponent && (seconds > 0 || mins > 0))
			{

				str.AppendPositiveInt(seconds);

				if (appendHourMinuteTags)
				{
					str.Append(secTag);
				}

				numComponentsUsed++;

				if (numComponentsUsed >= numberOfComponents)
				{
					return;
				}

			}
		}

		public static string ToTitleCase(this string me)
		{
			if (me.NotNullOrEmpty())
			{

				var cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
				return cultureInfo.TextInfo.ToTitleCase(me.ToLower());

			}
			else
			{
				return string.Empty;
			}
		}


		public static bool CompareCaseInsensitive(this string me, string other)
		{
			return string.Compare(me, other, true) == 0;
		}

		public static string SafeFormat(string format, params object[] args)
		{
			string result = format;
			if (format.NotNull())
			{
				try
				{
					result = string.Format(format, args);
				}
				catch (System.Exception e)
				{
					Debug.LogError("String SafeFormat Error: " + e.InnerException);
					result = format;
				}
			}
			else
			{
				result = string.Empty;
			}

			return result;
		}
	}
}