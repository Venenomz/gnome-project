﻿using UnityEngine;
using System.Collections;

namespace LobsterDog
{
	public static class ExtensionsObject
	{
		public static bool IsNull<T>(this T obj) where T : class
		{
			return (object)obj == null;
		}

		public static bool NotNull<T>(this T obj) where T : class
		{
			return (object)obj != null;
		}

		public static string ToJsonUnity(this object obj)
		{
			return JsonUtility.ToJson(obj);
		}

		public static T ToObjectFromJSONUnity<T>(this string str)
		{
			return JsonUtility.FromJson<T>(str);
		}

		public static T CloneViaJson<T>(this T obj)
		{
			return JsonUtility.FromJson<T>(JsonUtility.ToJson(obj));
		}


	}
}

