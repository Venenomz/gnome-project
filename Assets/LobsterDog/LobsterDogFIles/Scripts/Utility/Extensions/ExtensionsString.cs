using System;
using UnityEngine;
using System.Linq;

namespace LobsterDog
{
    [AddComponentMenu("LobsterDog/Utility/Extensions/ExtensionsString")]
	[DisallowMultipleComponent]
	public static class ExtensionsString
	{
		#region Methods

		public static string RemoveWhitespace(this string a_input)
		{
			return new string(a_input.Where(c => !Char.IsWhiteSpace(c))
				.ToArray());
		}

		#endregion
	}
}