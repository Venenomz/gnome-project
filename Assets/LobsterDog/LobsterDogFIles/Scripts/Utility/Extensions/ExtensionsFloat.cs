﻿using UnityEngine;
using System.Collections;

namespace LobsterDog
{
    public static class ExtensionsFloat
    {
        const float FULL_REVOLUTION = 360.0f;
        const float HALF_REVOLUTION = 180.0f;
        const float QUATER_REVOLUTION = 90.0f;

        public static float CosMap(float progress)
        {
            return (1f + Mathf.Cos(Mathf.PI + Mathf.PI * progress)) * 0.5f;
        }

        /// <summary>
        /// Lerp from me to target, according to progress (0.0f - 1.0f).
        /// </summary>
        /// <param name="me">Me</param>
        /// <param name="target">Target</param>
        /// <param name="progress">Progress</param>
        public static float Lerp(this float me, float target, float progress)
        {
            return me = Mathf.Lerp(me, target, progress);
        }

        public static float LerpHalfCos(this float me, float target, float progress)
        {
            return me + (target - me) * CosMap(progress);
        }

        public static float ToRadians(this float me)
        {
            return me * Mathf.PI / HALF_REVOLUTION;
        }

        public static float ToDegrees(this float me)
        {
            return me + QUATER_REVOLUTION % FULL_REVOLUTION;
        }


        public static float Squared(this float me)
        {
            return me * me;
        }

        public static float RoundDownToDecimalPlace(this float me, int decimalPlace)
        {
            float multiplier = Mathf.Pow(10, decimalPlace);
            me = (int)(me * multiplier);
            return me / multiplier;
        }

        //Like % but -3.5f % 1 will = -0.5f
        public static float ModWithNegative(this float me, float mod)
        {
            return (me.Abs() % mod) * me.Sign();
        }

        //positive rounds down, negative rounds to the lowest int
        public static int RoundToClosestInt(this float me)
        {
            if (me == 0)
            {
                return 0;
            }
            else
            {
                return (int)(me + (0.5f * me.Sign()));
            }
        }

        public static int RoundUpToInt(this float me)
        {
            return (int)System.Math.Ceiling(me);
        }

        public static Vector3 ToVectorInX(this float me)
        {
            return new Vector3(me, 0, 0);
        }

        public static Vector3 ToVectorInY(this float me)
        {
            return new Vector3(0, me, 0);
        }

        public static Vector3 ToVectorInZ(this float me)
        {
            return new Vector3(0, 0, me);
        }

        /// <summary>
        /// Rotates an angle towards another angle, via the shortest arc
        /// </summary>
        /// <param name="targetDegrees"></param>
        /// <param name="turnAmount"></param>
        public static float DegreesTurnToward(this float me, float targetDegrees, float turnAmount)
        {
            float toTarget = DegreesShortestArcTo(me, targetDegrees);


            if (Mathf.Abs(toTarget) < turnAmount)
            {
                return targetDegrees;
            }
            else
            {
                return me + (turnAmount * toTarget.Sign());
            }
        }

        /// <summary>
        /// Rotates an angle towards another angle, via the shortest arc
        /// </summary>
        /// <param name="targetDegrees"></param>
        /// <param name="turnAmount"></param>
        public static float DegreesLerpToward(this float me, float targetDegrees, float lerpFactor)
        {
            float toTarget = DegreesShortestArcTo(me, targetDegrees);

            return me + (toTarget * lerpFactor);
        }

        /// <summary>
        /// Like DegreesLerpToward, but with dt based damping.
        /// </summary>
        /// <param name="me"></param>
        /// <param name="targetDegrees"></param>
        /// <param name="lerpFactor"></param>
        /// <returns></returns>
        public static float DegreesDampToward(this float me, float targetDegrees, float lerpFactor, float dt)
        {
            float toTarget = DegreesShortestArcTo(me, targetDegrees);

            return me.DampToTarget(me + toTarget, lerpFactor, dt);
        }

        public static float DegreesShortestArcTo(this float me, float targetDegrees)
        {
            //make sure me isnt some crazy angle outside of range.
            me = me.Sign() * me.Abs() % FULL_REVOLUTION;
            targetDegrees = targetDegrees.Sign() * targetDegrees.Abs() % FULL_REVOLUTION;

            float toTarget = targetDegrees - me;

            if (toTarget > HALF_REVOLUTION)
            {
                toTarget -= FULL_REVOLUTION;
            }
            else if (toTarget < -HALF_REVOLUTION)
            {
                toTarget += FULL_REVOLUTION;
            }

            return toTarget;
        }

        public static float Wrap(this float me, float min, float max)
        {
            if (min == max)
            {
                return min;
            }
            else if (max < min)
            {
                float t = min;
                min = max;
                max = t;
            }

            float diff = max - min;
            while (me < min)
            {
                me += (diff);
            }
            while (me >= max)
            {
                me -= (diff);
            }
            return me;
        }

        public static void Clamp(ref float me, float min, float max)
        {
            if (me > max)
            {
                me = max;
            }
            else if (me < min)
            {
                me = min;
            }
        }

        /// <summary>
        /// Is inside min <= me <= max
        /// </summary>
        /// <param name="me"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static bool InsideRange(this float me, float min, float max)
        {
            return me >= min && me <= max;
        }

        public static bool OutsideRange(this float me, float min, float max)
        {
            return me < min || me > max;
        }

        public static float Clamp(this float me, float min, float max)
        {
            Clamp(ref me, min, max);
            return me;
        }

        public static float ClampToCeiling(this float me, float ceiling)
        {
            if (me > ceiling)
            {
                return ceiling;
            }

            return me;
        }

        public static float ClampToFloor(this float me, float floor)
        {
            if (me < floor)
            {
                return floor;
            }
            return me;
        }

        public static float Abs(this float me)
        {
            return Mathf.Abs(me);
        }

        public static float Sign(this float me)
        {
            return Mathf.Sign(me);
        }

        /// <summary>
        /// Maps a value to a normalised zero to one range based on supplied min and max.
        /// </summary>
        /// <param name="me"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float NormaliseFromRange(this float me, float min, float max)
        {
            return (me.Clamp(min, max) - min) / (max - min);
        }

        public static float Map(this float me, float inputMin, float inputMax, float outPutMin, float outputMax)
        {
            return outPutMin + (me.NormaliseFromRange(inputMin, inputMax) * (outputMax - outPutMin));
        }

        public static float MapUnbounded(this float me, float inputMin, float inputMax, float outPutMin, float outputMax)
        {
            return outPutMin + ((me - inputMin) / (inputMax - inputMin)) * (outputMax - outPutMin);
        }

        public static float Map(this float me, float inputMin, float inputMax, float outPutMin, float outputMax, System.Func<float, float, float, float> easeFunc)
        {
            return easeFunc(outPutMin, outputMax, me.NormaliseFromRange(inputMin, inputMax));
        }

        //Assumes the input to be zero to one
        public static float MapNormal(this float me, float outPutMin, float outputMax, System.Func<float, float, float, float> easeFunc)
        {
            return easeFunc(outPutMin, outputMax, me);
        }

        public static float MapNormalEase(this float me, System.Func<float, float, float, float> easeFunc)
        {
            return easeFunc(0f, 1f, me);
        }

        public static int FloatToInt(this float me)
        {
            return (int)me;
        }

        public static int MapToInt(this float me, float inputMin, float inputMax, int outPutMin, int outputMax)
        {
            return (int)(outPutMin + (me.NormaliseFromRange(inputMin, inputMax) * (outputMax - outPutMin)));
        }

        public static float ToPowerOf(this float me, float power)
        {
            return Mathf.Pow(me, power);
        }

        /// <summary>
        /// Is this less than a random number 0 ~ 1.
        /// </summary>
        /// <param name="me"></param>
        /// <returns>Returns true if a random number 0 ~ 1 is less than this.</returns>
        public static bool FractionChance(this float me)
        {
            return Random.value < me;
        }

        const float FLOAT_ERROR = 0.000001f;
        public static int ToIntIgnoreError(this float me)
        {
            return me.RoundToClosestInt();
        }
        /// <summary>
        /// Adds a random amount to this float.
        /// </summary>
        /// <param name="me"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static float PlusRandomAmount(this float me, float range)
        {
            return me + Random.value * range;
        }

        /// <summary>
        /// x => x - range ~> x + range
        /// </summary>
        /// <param name="me"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static float PlusMinusRandomAmount(this float me, float range)
        {
            return me - range + (Random.value * range * 2);
        }

        public static float PlusMinusRandomAmount(this float me, float range, float excludeMiddle)
        {
            float amount = excludeMiddle + (Random.value * (range - excludeMiddle));

            return me + Random.value > 0.5f ? amount : -amount;
        }

        // Smoothing rate dictates the proportion of source remaining after one 60th of a second.
        public static float DampToTarget(this float me, float target, float smoothing, float dt)
        {
            return Mathf.Lerp(me, target, 1 - Mathf.Pow(smoothing, dt * 60f));
        }

        public static float DampToZero(this float me, float smoothing, float dt)
        {
            return me * Mathf.Pow(smoothing, dt * 60f);
        }

        public static float SinOfDegrees(this float me)
        {
            return Mathf.Sin(me * Mathf.Deg2Rad);
        }

        /// <summary>
        /// Returns the Sin of me, mapped to 0 -> 1f
        /// </summary>
        /// <param name="me"></param>
        /// <returns></returns>
        public static float SinOfDegreesPositive(this float me)
        {
            return 0.5f + Mathf.Sin(me * Mathf.Deg2Rad) * 0.5f;
        }

        public static float CosOfDegrees(this float me)
        {
            return Mathf.Cos(me * Mathf.Deg2Rad);
        }

        public static Vector2 RadianToVector2(this float me)
        {
            return new Vector2(Mathf.Cos(me), Mathf.Sin(me));
        }

        public static Vector2 DegreesToVector(this float me)
        {
            return RadianToVector2(me * Mathf.Deg2Rad);
        }
    }
}
