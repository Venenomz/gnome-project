using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	[AddComponentMenu("LobsterDog/Utility/QuitOnEscape")]
	[DisallowMultipleComponent]
	public class QuitOnEscape : MonoBehaviour
	{
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
}