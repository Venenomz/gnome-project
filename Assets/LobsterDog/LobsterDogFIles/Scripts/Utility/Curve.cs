using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [System.Serializable]
    public class Curve
    {
        [SerializeField] AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [SerializeField] MinMaxFloat range = null;

        public float Min { get { return range.Min; } }

        public float Max { get { return range.Max; } }

        public enum WrapMode { Clamp, Loop, PingPong };

        [SerializeField] WrapMode wrapMode = WrapMode.Clamp;

        public enum TimerMode { Time, Speed };

        [SerializeField] TimerMode timerMode = TimerMode.Time;

        //this is either the speed or the total time taken to go between 0 and 1, depending on the timerMode.
        [SerializeField] float timerVal = 1;

        float startTime = -1;

        void Start()
        {
            startTime = Time.fixedTime;
            SetWrapMode();
        }

        void SetWrapMode()
        {
            switch (wrapMode)
            {
                case WrapMode.Clamp:
                    {
                        curve.postWrapMode = UnityEngine.WrapMode.Clamp;
                        break;
                    }
                case WrapMode.Loop:
                    {
                        curve.postWrapMode = UnityEngine.WrapMode.Loop;
                        break;
                    }
                case WrapMode.PingPong:
                    {
                        curve.postWrapMode = UnityEngine.WrapMode.PingPong;
                        break;
                    }
            }
        }

        public float GetPoint(float01 a_value)
        {
            return GetPoint(a_value.Value);
        }

        float GetPoint(float a_value)
        {
            return range.Lerp(curve.Evaluate(a_value));
        }

        public void Restart()
        {
            Start();
        }

        public float Value()
        {
            if (startTime == -1)
            {
                Start();
            }

            float val = Time.fixedTime - startTime;

            switch (timerMode)
            {
                case TimerMode.Time:
                    {
                        val /= timerVal;
                        break;
                    }
                case TimerMode.Speed:
                    {
                        val *= timerVal;
                        break;
                    }
            }

            return GetPoint(val);
        }
	}
}