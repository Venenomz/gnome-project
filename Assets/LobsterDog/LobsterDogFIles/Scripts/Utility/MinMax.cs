using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    [System.Serializable]
	public class MinMaxInt
	{
        public int Min = 0;
        public int Max = 0;

        public int Clamp(int a_value)
        {
            return Mathf.Clamp(a_value, Min, Max);
        }

        public int Lerp(float01 a_value)
        {
            return Mathf.RoundToInt(Mathf.Lerp(Min, Max, a_value.Value));
        }

        public float01 InverseLerp(float a_value)
        {
            return (a_value - (float)Min) / (float)(Max - Min);
        }

        public int Random()
        {
            return UnityEngine.Random.Range(Min, Max);
        }
    }

    [System.Serializable]
    public class MinMaxFloat
    {
        public float Min = 0.0f;
        public float Max = 0.0f;

        public float Clamp(float a_value)
        {
            return Mathf.Clamp(a_value, Min, Max);
        }

        public float Lerp(float01 a_value)
        {
            return Mathf.Lerp(Min, Max, a_value.Value);
        }

        public float01 InverseLerp(float a_value)
        {
            return (a_value - Min) / (Max - Min);
        }

        public float Random()
        {
            return UnityEngine.Random.Range(Min, Max);
        }
    }
}