using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    public class LazyRef<T> where T : MonoBehaviour
    {
        T reference = null;

        public T Ref
        {
            get
            {
                if (reference == null)
                {
                    reference = MonoBehaviour.FindObjectOfType<T>();
                }
                return reference;
            }
        }
    }
}