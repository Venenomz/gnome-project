using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    //There should be a StoredAudio for each audio type to be used in the game. Each should be added to a separate game object, a child of the AudioManager.
    //This script will find any AudioSource on its own or its childrens gameobject and add them to it's audio list. When prompted to play it will select from its audio list using the chosen GroupSelectionType.
    //This is to allow for variations on sounds
    [AddComponentMenu("LobsterDog/Audio/StoredAudio")]
    [DisallowMultipleComponent]
	public class StoredAudio : MonoBehaviour
	{
        public enum AudioType { SFX, MUSIC }

        [SerializeField]  AudioType thisAudioType = AudioType.SFX;
        public AudioType ThisAudioType { get { return thisAudioType; } }

        //What method this StoredAudio should use to select the next AudioSource
        public enum GroupSelectionType
        {
            OnlyFirst, //Always pick the first one in the list
            Random, //Choose completely at random
            ShuffleBag, //Choose randomly, but remove from selection once chosen. Reset when all are used.
            Series //Choose files in order (based on inspector heirarchy)
        }

        [SerializeField] GroupSelectionType groupSelectionType = GroupSelectionType.ShuffleBag;

        List<SpawnedAudio> spawnedAudios = new List<SpawnedAudio>(); //List of current instantiated audios
        List<AudioSource> audioClips = new List<AudioSource>(); //List of audio clips that could be played by this stored audio (based on GroupSelectionType)

        [SerializeField] int instancesLimit = 100; //How many active instances of the audio are allowed at once?

        int groupSelectionSeriesCount = 0;
        List<int> shuffleBag = new List<int>();

        List<float> baseVolumes = new List<float>(); //Keeps track of the base volumes of all audio, ignoring users Audio settings

        [Button("Test Audio")]
        void TestAudio()
        {
            PlayAudio(true);
        }

        void Awake()
        {
            FillAudioList();

            for (int i = 0; i < audioClips.Count; ++i)
            {
                baseVolumes.Add(audioClips[i].volume);
            }
        }

        void FillAudioList()
        {
            if (audioClips.Count == 0)
            {
                AudioSource[] childAudios = GetComponentsInChildren<AudioSource>();
                {
                    for (int i = 0; i < childAudios.Length; ++i)
                    {
                        audioClips.Add(childAudios[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Select which Audio to play based on the GroupSelectionType.
        /// </summary>
        /// <returns>The chosen AudioSource.</returns>
        AudioSource ChooseAudio()
        {
            int audioNumber = 0;

            switch (groupSelectionType)
            {
                case GroupSelectionType.OnlyFirst:
                    {
                        //Just use the first one
                        break;
                    }
                case GroupSelectionType.Random:
                    {
                        audioNumber = Random.Range(0, audioClips.Count);
                        break;
                    }
                case GroupSelectionType.ShuffleBag:
                    {
                        int maxTimes = 30;
                        int rand = 0;
                        do
                        {
                            rand = Random.Range(0, audioClips.Count);
                            --maxTimes;
                        } while (shuffleBag.Contains(rand) && maxTimes > 0);

                        audioNumber = rand;

                        if (shuffleBag.Count == audioClips.Count - 1 || maxTimes <= 0)
                        {
                            shuffleBag.Clear();
                        }

                        shuffleBag.Add(rand); //This is added afterwards to make sure the same sound is not played twice in a row upon reset of shufflebag
                        break;
                    }
                case GroupSelectionType.Series:
                    {
                        groupSelectionSeriesCount = (groupSelectionSeriesCount + 1) % audioClips.Count;
                        audioNumber = groupSelectionSeriesCount;
                        break;
                    }
            }

            return audioClips[audioNumber];
        }

        /// <summary>
        /// Plays the StoredAudio. Used for global sounds with no source (UI, Music etc).
        /// </summary>
        /// <param name="a_override">Set to true if the sound should restart if it is already playing.</param>
        public void PlayAudio(bool a_override)
        {
            if (audioClips.Count == 0)
            {
                return;
            }

            if (!IsPlaying() || a_override)
            {
                ChooseAudio().Play();
            }
        }

        /// <summary>
        /// Spawns a SpawnedAudio in the position given. The SpawnedAudio is destroyed when it stops playing or its volume is 0.
        /// </summary>
        /// <param name="a_position">The position to spawn in.</param>
        /// <param name="a_volumeMultiplier">Set a base volume multiplier for this particular instance.</param>
        /// <returns>The SpawnedAudio created.</returns>
        public SpawnedAudio PlayAudio(Vector3 a_position, float a_volumeMultiplier)
        {
            GameObject newSound = Instantiate(ChooseAudio().gameObject);
            Destroy(newSound.GetComponent<StoredAudio>());

            for (int i = 0; i < newSound.transform.childCount; ++i)
            {
                Destroy(newSound.transform.GetChild(i).gameObject);
            }

            newSound.transform.position = a_position;
            newSound.transform.parent = transform;
            newSound.GetComponent<AudioSource>().volume *= a_volumeMultiplier;

            spawnedAudios.Add(newSound.AddComponent<SpawnedAudio>());
            spawnedAudios[spawnedAudios.Count - 1].Initialise();
            if (spawnedAudios.Count > instancesLimit)
            {
                spawnedAudios[spawnedAudios.Count - 1].InstanceLimit();
            }

            return newSound.GetComponent<SpawnedAudio>();
        }

        /// <summary>
        /// Stops both StoredAudio and any SpawnedAudios that are currently playing.
        /// </summary>
        public void StopAudio()
        {
            for (int i = 0; i < spawnedAudios.Count; ++i)
            {
                spawnedAudios[i].StopAudio();
            }

            for (int i = 0; i < audioClips.Count; ++i)
            {
                audioClips[i].Stop();
            }
        }

        /// <summary>
        /// Called by a SpawnedAudio when it has stopped playing
        /// </summary>
        /// <param name="a_spawnedAudio">The SpawnedAudio to deregister</param>
        public void DeregisterSpawnedAudio(SpawnedAudio a_spawnedAudio)
        {
            if (spawnedAudios.Contains(a_spawnedAudio))
            {
                spawnedAudios.Remove(a_spawnedAudio);
            }
        }

        /// <summary>
        /// Check if either the StoredAudio or any SpawnedAudios are currently playing.
        /// </summary>
        /// <returns>Whether or not the audio is playing.</returns>
        public bool IsPlaying()
        {
            if (spawnedAudios.Count > 0)
            {
                return true;
            }

            for (int i = 0; i < audioClips.Count; ++i)
            {
                if (audioClips[i].isPlaying)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Hard set the volume value of the stored audio.
        /// </summary>
        /// <param name="a_volume">The volume to hard set the stored audios to (before volume settings are applied).</param>
        /// <param name="a_index">The index of the stored audio to set. Defaults to all.</param>
        public void SetVolumes(float a_volume, int a_index = -1)
        {
            if (a_index == -1)
            {
                for (int i = 0; i < audioClips.Count; ++i)
                {
                    baseVolumes[i] = a_volume;
                }
            }
            else
            {
                baseVolumes[a_index] = a_volume;
            }

            UpdateVolumes();
        }

        public float GetVolume(int a_index)
        {
            return baseVolumes[a_index];
        }

        void Update()
        {
            //Check if any SpawnedAudios in the queue are ready to be un-instance-limited.
            if (spawnedAudios.Count >= instancesLimit)
            {
                for (int i = 0; i < instancesLimit; ++i)
                {
                    if (spawnedAudios[i].InstanceLimited)
                    {
                        spawnedAudios[i].UninstanceLimit();
                    }
                }
            }
        }

        /// <summary>
        /// Applies changes to AudioManager volume multipliers (ie volume settings). Should generally only be called by the AudioManager after settings are changed.
        /// </summary>
        public void UpdateVolumes()
        {
            for (int i = 0; i < audioClips.Count; ++i)
            {
                audioClips[i].volume = baseVolumes[i] * AudioManager.Instance.SettingsVolumeMultiplier(thisAudioType);
            }
        }
    }
}