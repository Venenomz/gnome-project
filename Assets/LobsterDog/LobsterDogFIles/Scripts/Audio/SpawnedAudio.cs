using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
    //This is an instance of a StoredAudio spawned at a particular place. It should only be interected with by it's StoredAudio and is destroyed after the sound has played (or when it's volume is zero).
    [AddComponentMenu("LobsterDog/Audio/SpawnedAudio")]
    [DisallowMultipleComponent]
	public class SpawnedAudio : MonoBehaviour
	{
        #region Reference Variables
        StoredAudio storedAudio;
        #endregion

        public AudioSource ThisAudioSource
        {
            get { return thisAudioSource; }
            private set { thisAudioSource = value; }
        }
        [SerializeField] AudioSource thisAudioSource;

        public bool InstanceLimited
        {
            get { return instanceLimited; }
            private set { instanceLimited = value; }
        }
        [SerializeField] bool instanceLimited = false;

        float fadeSpeed = 0;
        float fadeTarget = 0;
        float originalVolume;
        float currentVolume;

        public void Initialise()
        {
            ThisAudioSource = GetComponent<AudioSource>();
            ThisAudioSource.Play();
            storedAudio = transform.parent.GetComponent<StoredAudio>();
            originalVolume = ThisAudioSource.volume;
            currentVolume = ThisAudioSource.volume;
        }

        void Update()
        {
            if (!ThisAudioSource.isPlaying)
            {
                storedAudio.DeregisterSpawnedAudio(this);
                Destroy(gameObject);
            }

            if (currentVolume <= 0)
            {
                ThisAudioSource.Stop();
            }
            else if (fadeSpeed != 0)
            {
                currentVolume += fadeTarget * Time.deltaTime;

                if (currentVolume < fadeTarget && fadeSpeed < 0)
                {
                    currentVolume = fadeTarget;
                    fadeSpeed = 0;
                }
                else if (currentVolume > fadeTarget && fadeSpeed > 0)
                {
                    currentVolume = fadeTarget;
                    fadeSpeed = 0;
                }
            }

            ApplyCurrentVolume();
        }

        void ApplyCurrentVolume()
        {
            if (!InstanceLimited)
            {
                ThisAudioSource.volume = currentVolume * AudioManager.Instance.SettingsVolumeMultiplier(storedAudio.ThisAudioType);
            }
        }

        public void UninstanceLimit()
        {
            InstanceLimited = false;
            ApplyCurrentVolume();
        }

        public void InstanceLimit()
        {
            InstanceLimited = true;
            ThisAudioSource.volume = 0;
        }

        public void FadeOut(float a_seconds, float a_percent = 1.0f)
        {
            fadeTarget = originalVolume - (originalVolume * a_percent);

            fadeSpeed = -1 * ((currentVolume - fadeTarget) / a_seconds);
        }

        public void FadeToOriginal(float a_seconds, bool a_setToZero = false)
        {
            if (a_setToZero)
            {
                currentVolume = 0.01f;
            }

            fadeTarget = originalVolume;

            fadeSpeed = -1 * (currentVolume - fadeTarget) / a_seconds;
        }

        public void StopAudio()
        {
            ThisAudioSource.Stop();
        }

        public bool IsPlaying()
        {
            return ThisAudioSource.isPlaying;
        }
    }
}