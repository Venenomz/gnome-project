using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;
using System.IO;

namespace LobsterDog
{
	public class JSONStoredScriptable : ScriptableObject
	{
		enum Location { StreamingAssets, PersistentDataPath }

        #region Fields
        [SerializeField, BoxGroup("")] string fileName = "config";
        [SerializeField, BoxGroup("")] Location location = Location.PersistentDataPath;
        #endregion


        #region Properties
        string directory
        {
            get
            {
                switch (location)
                {
                    case Location.PersistentDataPath:
                    {
                        return Application.persistentDataPath;
                    }
                    case Location.StreamingAssets:
                    {
                        return Application.streamingAssetsPath;
                    }
                    default:
                    {
                        return "";
                    }
                }
            }
        }

        protected string dataPath { get { return directory + "\\" + fileName + ".json"; } }
		#endregion
	}
}