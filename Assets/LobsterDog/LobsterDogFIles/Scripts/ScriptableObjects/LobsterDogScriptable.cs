using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LobsterDog
{
	public class LobsterDogScriptable : ScriptableObject
	{
#if UNITY_EDITOR
        [SerializeField, TextArea(3, 10), BoxGroup("Internal")] string description;
#endif

        /// <summary>
        /// Prints details about this scritable object to the console. Justn ame by default. Overridde PrintText to change.
        /// </summary>
        /// <param name="a_additionalText"></param>
        public void Print(string a_additionalText = "")
        {
            DebugTools.Log(a_additionalText + ": " + PrintText());
        }
        
        public virtual string PrintText()
        {
            return name;
        }
    }
}