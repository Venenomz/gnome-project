using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using UnityEngine.EventSystems;

namespace GnomeProject
{
    public class InputManager : Singleton<InputManager>
    {
        #region Variables

        private List<LobsterDogTouch> _touches;

        private List<int> _currentTouchIDs;

        private const int MAX_TOUCH_COUNT = 10;

        private Vector3 _previousMousePositon = Vector3.zero;

        private bool _inited = false;

        #endregion

        #region Properties

        #endregion

        #region Methods

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_inited)
            {
                HandleInput();
            }
        }

        internal void Init()
        {
            _touches = new List<LobsterDogTouch>(MAX_TOUCH_COUNT);
            _currentTouchIDs = new List<int>(MAX_TOUCH_COUNT);
        }

        private void HandleInput()
        {
            List<int> currentTouchIds = new List<int>();

            //Try mouse input if no touches exist
            if (Input.touchCount == 0 && Input.GetMouseButton(0))
            {
                Vector2 mouseDelta = Input.mousePosition - _previousMousePositon;

                _previousMousePositon = mouseDelta;

                TouchPhase phase = TouchPhase.Stationary;

                if (Input.GetMouseButtonDown(0))
                {
                    phase = TouchPhase.Began;
                }
                else if (Vector3.Magnitude(Input.mousePosition - _previousMousePositon) > 0.01f)
                {
                    phase = TouchPhase.Moved;
                }

                LobsterDogTouch mouseTouch = FindTouch(-1, Input.mousePosition);

                if (mouseTouch != null)
                {
                    currentTouchIds.Add(-1);
                    UpdateTouch(mouseTouch, phase, Input.mousePosition);
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                LobsterDogTouch mouseTouch = FindTouch(-1);

                if (mouseTouch != null)
                    UpdateTouch(mouseTouch, TouchPhase.Ended, Input.mousePosition);
            }

            foreach (Touch touch in Input.touches)
            {
                LobsterDogTouch matchingTouch = FindTouch(touch.fingerId, touch.position);

                currentTouchIds.Add(touch.fingerId);

                if (matchingTouch != null)
                    UpdateTouch(matchingTouch, touch.phase, touch.position);
            }

            //See if we have any touches that are no longer really there (ie if touchphase.ended was missed)
            List<int> touchesToRemove = new List<int>();

            for (int i = 0; i < _touches.Count; ++i)
            {
                if (!currentTouchIds.Contains(_touches[i].TouchID))
                {
                    _touches[i].Update(TouchPhase.Ended);
                    touchesToRemove.Add(_touches[i].TouchID);
                }
            }

            if (touchesToRemove.Count > 0)
                _touches.RemoveAll(item => touchesToRemove.Contains(item.TouchID));
        }

        internal void OnTouchUp(int touchId, Vector2 screenPosition)
        {
            if (_currentTouchIDs.Contains(touchId) && _currentTouchIDs.Count > 0)
            {
                bool resetLastDist = _currentTouchIDs[0] == touchId || _currentTouchIDs[1] == touchId;

                if (FindTouch(touchId).IsTap && !FindTouch(touchId).Moved)
                {
                    OnTap(screenPosition);

                    if (Utils.UnityEditor())
                        Debug.Log("Tap detected from finger: " + touchId + ". Time: " + (FindTouch(touchId).TouchTime));
                }

                _currentTouchIDs.Remove(touchId);

                // Game.MainCamera.Rotate(0);

                if (resetLastDist)
                {
                    //ResetLastTouchDistance();
                    //ResetLastTouchAngle();
                }
            }
        }

        private void OnTap(Vector2 screenPosition)
        {
            AI_BaseClass found = GetTappedAI(screenPosition);

            if (found.NotNull())
            {
                // TODO: Fancy stuff

                Game.GameCamera.SetTarget(found, 0, 0, 1f, true);

                Game.UIManager.SwitchMenuState("Interaction");
            }
        }

        private AI_BaseClass GetTappedAI(Vector2 tapPosition)
        {
            Ray ray = Game.GameCamera.Cam.ScreenPointToRay(tapPosition);

            if (Physics.Raycast(ray.origin, ray.direction, out RaycastHit hit, Mathf.Infinity, Game.UIManager.InteractionLayer))
            {
                if (hit.collider.TryGetComponent<AI_BaseClass>(out var ai))
                    return ai;
            }

            return null;
        }

        internal void OnSwipe(Vector2 screenPosition, Vector2 delta)
        {
            if (_currentTouchIDs.Count == 1)
            {
                Game.GameCamera.OnSwipe(delta);
            }
            else if (_currentTouchIDs.Count >= 2)
            {
                //RotateCamera(FindTouch(_currentTouchIDs[0]).ScreenPos, FindTouch(_currentTouchIDs[1]).ScreenPos);
                //ZoomCamera(FindTouch(_currentTouchIDs[0]).ScreenPos, FindTouch(_currentTouchIDs[1]).ScreenPos);
            }
        }

        internal void OnTouchDown(int touchId, Vector2 screenPosition)
        {
            if (IsPositionOverUIObject(screenPosition))
                return;

            _currentTouchIDs.Add(touchId);

            Game.GameCamera.OnSwipe(Vector2.zero);
        }

        //Returns 'true' if we touched or hovering on Unity UI element.
        private bool IsPositionOverUIObject(Vector2 screenPosition)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
            {
                position = screenPosition
            };

            List<RaycastResult> results = new List<RaycastResult>();

            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            return results.Count > 0;
        }

        public LobsterDogTouch FindTouch(int touchID)
        {
            LobsterDogTouch matchingTouch = null;

            for (int i = 0; i < _touches.Count; ++i)
            {
                if (_touches[i].TouchID == touchID)
                {
                    matchingTouch = _touches[i];
                    break;
                }
            }

            return matchingTouch;
        }

        private LobsterDogTouch FindTouch(int touchID, Vector2 touchPos, bool createIfNotFound = true)
        {
            LobsterDogTouch matchingTouch = FindTouch(touchID);

            if (matchingTouch == null && createIfNotFound)
            {
                matchingTouch = new LobsterDogTouch(touchID);
                _touches.Add(matchingTouch);
            }

            return matchingTouch;
        }

        void UpdateTouch(LobsterDogTouch touch, TouchPhase touchPhase, Vector3 screenPosition)
        {
            touch.Update(touchPhase, screenPosition);

            if (touchPhase == TouchPhase.Ended || touchPhase == TouchPhase.Canceled)
                _touches.Remove(touch);
        }

        #endregion
    }

    [System.Serializable]
    public class LobsterDogTouch
    {
        int _touchID;
        Vector2 _lastScreenPos = Vector2.zero;
        Vector2 _screenPos = Vector2.zero;

        float _touchTimeBegan;
        float _touchTimeEnded;

        bool _moved = false;

        const float TAP_TIME_THRESHOLD = 0.2f;

        public int TouchID { get { return _touchID; } }

        public Vector2 Delta { get { return _screenPos - _lastScreenPos; } }

        public Vector2 ScreenPos { get { return _lastScreenPos; } }

        public float TouchTime { get { return _touchTimeEnded; } }

        public bool Moved { get { return _moved; } }

        public bool IsTap { get { return _touchTimeEnded <= TAP_TIME_THRESHOLD; } }

        public LobsterDogTouch(int touchID)
        {
            _touchID = touchID;
        }

        public void Update(TouchPhase phase)
        {
            Update(phase, _screenPos);
        }

        public void Update(TouchPhase phase, Vector2 screenPosition)
        {
            _screenPos = screenPosition;

            switch (phase)
            {
                case TouchPhase.Began:
                    {
                        _touchTimeBegan = Time.time;

                        Game.InputManager.OnTouchDown(_touchID, screenPosition);

                        break;
                    }
                case TouchPhase.Moved:
                    {
                        _moved = true;

                        Game.InputManager.OnSwipe(screenPosition, Delta);

                        break;
                    }
                case TouchPhase.Ended:
                    {
                        _touchTimeEnded = Time.time - _touchTimeBegan;

                        Game.InputManager.OnTouchUp(_touchID, screenPosition);

                        break;
                    }
            }

            _lastScreenPos = screenPosition;
        }
    }
}

