using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

namespace GnomeProject
{
    [AddComponentMenu("Gnome Project/AI/Goals/Goap UI/Goap_UI")]
	[DisallowMultipleComponent]
	public class Goap_UI : MonoBehaviour
	{
		#region Reference Fields

		[SerializeField, FoldoutGroup("References")]
		private GameObject _goalPrefab;

		[SerializeField, FoldoutGroup("References")]
		private RectTransform _goalRoot;

		Dictionary<MonoBehaviour, Goal_UI> _displayedGoals = new Dictionary<MonoBehaviour, Goal_UI>();
		#endregion

		#region Methods

		internal void UpdateGoal(MonoBehaviour goal, string goalName, string goalStatus, float priority)
        {
			if (!_displayedGoals.ContainsKey(goal))
				_displayedGoals[goal] = Instantiate(_goalPrefab, Vector3.zero, Quaternion.identity, _goalRoot).GetComponent<Goal_UI>();

			_displayedGoals[goal].UpdateGoalInfo(goalName, goalStatus, priority);
        }

		#endregion
	}
}