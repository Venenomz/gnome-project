using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Goals/Human Goals/HumanGoal_Idle")]
	[DisallowMultipleComponent]
	public class HumanGoal_Idle : Goal_Base
	{
		#region Reference Fields

		[SerializeField, FoldoutGroup("References")]
		private int _priority = 10;

        #endregion

        #region Methods

        public override int CalculatePriority()
        {
            return _priority;
        }

        public override bool CanRun()
        {
            return true;
        }

        #endregion
    }
}