using LobsterDog;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GnomeProject
{
    [AddComponentMenu("Gnome Project/AI/Goals/Human Goals/Goal_Investigate")]
    [DisallowMultipleComponent]
    public class HumanGoal_Investigate : Goal_Base
    {
        #region Reference Fields

        [SerializeField, FoldoutGroup("Goal Variables")]
        private int _investigatePrority = 60;

        [SerializeField, FoldoutGroup("Goal Variables")]
        private float _minAwarenessToInvestigate = 1.5f;

        [SerializeField, FoldoutGroup("Goal Variables")]
        private float _awarenessToStopInvestigate = 1.5f;

        private DetectableTarget _currentTarget;

        private int _currentPriority = 0;

        #endregion

        #region Fields

        internal Vector3 MoveTarget => _currentTarget.NotNull() ? _currentTarget.transform.position : transform.position;

        #endregion

        #region Properties
        #endregion

        #region Methods

        public override void OnTickGoal()
        {
            _currentPriority = 0;

            if (_sensors.ActiveTargets.IsNull() || _sensors.ActiveTargets.Count == 0)
                return;

            if (_currentTarget.NotNull())
            {
                // check if the current target is still being sensed
                foreach (var candidate in _sensors.ActiveTargets.Values)
                {
                    if (candidate.DetectableTarget == _currentTarget)
                    {
                        _currentPriority = candidate.Awareness < _awarenessToStopInvestigate ? 0 : _investigatePrority;

                        return;
                    }
                }

                _currentTarget = null;
            }

            // acquire a new target if possible
            foreach (var candidate in _sensors.ActiveTargets.Values)
            {
                if (candidate.Awareness >= _minAwarenessToInvestigate)
                {
                    _currentTarget = candidate.DetectableTarget;
                    _currentPriority = _investigatePrority;

                    return;
                }
            }
        }

        public override void OnGoalDeactivated()
        {
            base.OnGoalDeactivated();

            _currentTarget = null;
        }

        public override int CalculatePriority()
        {
            return _currentPriority;
        }

        public override bool CanRun()
        {
            // no targets :(
            if (_sensors.ActiveTargets.IsNull() || _sensors.ActiveTargets.Count == 0)
                return false;

            foreach (var candidate in _sensors.ActiveTargets.Values)
                if (candidate.Awareness >= _minAwarenessToInvestigate)
                    return true;

            return false;
        }

        #endregion
    }
}