using Sirenix.OdinInspector;
using UnityEngine;

namespace GnomeProject
{
    [AddComponentMenu("Gnome Project/AI/Goals/Human Goals/HumanGoal_Wander")]
	[DisallowMultipleComponent]
	public class HumanGoal_Wander : Goal_Base
	{
		#region Reference Fields

		[SerializeField, FoldoutGroup("References")]
		private int _wanderPriority = 30;

		[SerializeField, FoldoutGroup("References")]
		private float _priorityBuildRate = 1f;

		[SerializeField, FoldoutGroup("References")]
		private float _priorityDecayRate = 0.1f;

		[SerializeField, FoldoutGroup("References"), ReadOnly]
		private float _currentPriority = 0f;

        #endregion

        #region Methods

        public override void OnTickGoal()
        {
            if (_connectedAI.IsMoving)
            {
				_currentPriority -= _priorityDecayRate * Time.deltaTime;
            }
            else
            {
				_currentPriority += _priorityBuildRate * Time.deltaTime;
            }
        }

        public override void OnGoalActivated(Action_Base action)
        {
            base.OnGoalActivated(action);

            _currentPriority = _wanderPriority;
        }

        public override int CalculatePriority()
        {
            return Mathf.FloorToInt(_currentPriority);
        }

        public override bool CanRun()
        {
            return true;
        }

        #endregion
    }
}