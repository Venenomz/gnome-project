using UnityEngine;

namespace GnomeProject
{
    public interface IGoal
    {
		int CalculatePriority();
		bool CanRun();
		void OnTickGoal();
		void OnGoalActivated(Action_Base _linkedGoal);
		void OnGoalDeactivated();
    }

	[AddComponentMenu("Gnome Project/AI/Goals/Goal_Base")]
	[DisallowMultipleComponent]
	public class Goal_Base : MonoBehaviour, IGoal
	{
		#region Reference Fields

		protected AI_BaseClass _connectedAI;
		protected Action_Base _linkedAction;
		protected AwarenessSystem _sensors;
		protected Goap_UI _debugUI;

        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        private void Awake()
        {
			_connectedAI = GetComponent<AI_BaseClass>();
			_sensors = GetComponent<AwarenessSystem>();
        }

        private void Start()
        {
			_debugUI = FindObjectOfType<Goap_UI>();
        }

        private void Update()
        {
            OnTickGoal();

            _debugUI.UpdateGoal(this, GetType().Name, _linkedAction ? "Running" : "On Hold", CalculatePriority());
        }

        public virtual int CalculatePriority()
        {
			return -1;
        }

		public virtual bool CanRun()
        {
			return false;
        }

		public virtual void OnTickGoal()
        {

        }

		public virtual void OnGoalActivated(Action_Base action)
        {
			_linkedAction = action;
        }

		public virtual void OnGoalDeactivated()
        {
			_linkedAction = null;
        }

		#endregion
	}
}