using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GnomeProject
{
    [AddComponentMenu("Gnome Project/AI/Goals/Goal_UI")]
	[DisallowMultipleComponent]
	public class Goal_UI : MonoBehaviour
	{
		#region Reference Fields

		[SerializeField, FoldoutGroup("References")]
		private TMP_Text _name;

		[SerializeField, FoldoutGroup("References")]
		private Slider _prioritySlider;

		[SerializeField, FoldoutGroup("References")]
		private TMP_Text _status;

		#endregion

		#region Methods

		internal void UpdateGoalInfo(string goalName, string status, float priority)
        {
			_name.text = goalName;
			_status.text = status;
			_prioritySlider.value = priority;
        }

		#endregion
	}
}