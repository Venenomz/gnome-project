using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Actions/Action_Base")]
	[DisallowMultipleComponent]
	public class Action_Base : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		protected AI_BaseClass _connectedAI;
		protected Goal_Base _linkedGoal;

        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        private void Awake()
        {
            _connectedAI = GetComponent<AI_BaseClass>();
        }

        internal virtual List<System.Type> GetSupportedGoals()
        {
            return null;
        }

        internal virtual float GetCost()
        {
            return 0f;
        }

        internal virtual void OnActivated(Goal_Base goal)
        {
            _linkedGoal = goal;
        }

        internal virtual void OnDeactivated()
        {
            _linkedGoal = null;
        }

        internal virtual void OnTick()
        {

        }

        #endregion
    }
}