using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Actions/Human Actions/HumanAction_Investigate")]
	[DisallowMultipleComponent]
	public class HumanAction_Investigate : Action_Base
	{
		#region Reference Fields

		List<System.Type> _supportedGoals = new List<System.Type>(new System.Type[] { typeof(HumanGoal_Investigate) });

        private HumanGoal_Investigate _investigateGoal;

        #endregion

        #region Methods

        internal override List<Type> GetSupportedGoals()
        {
            return _supportedGoals;
        }

        internal override float GetCost()
        {
            return 0f;
        }

        internal override void OnActivated(Goal_Base goal)
        {
            base.OnActivated(goal);

            _investigateGoal = (HumanGoal_Investigate)_linkedGoal;

            _connectedAI.StartMove(_investigateGoal.MoveTarget);
        }

        internal override void OnDeactivated()
        {
            base.OnDeactivated();

            _investigateGoal = null;
        }

        internal override void OnTick()
        {
            _connectedAI.StartMove(_investigateGoal.MoveTarget);
        }

        #endregion
    }
}