using System;
using System.Collections.Generic;
using UnityEngine;

namespace GnomeProject
{
    [AddComponentMenu("Gnome Project/AI/Actions/Human Actions/HumanAction_Idle")]
	[DisallowMultipleComponent]
	public class HumanAction_Idle : Action_Base
	{
		#region Reference Fields

		List<System.Type> _supportedGoals = new List<System.Type>(new System.Type[] { typeof(HumanGoal_Idle) });

        #endregion

        #region Methods

        internal override List<Type> GetSupportedGoals()
        {
            return _supportedGoals;
        }

        internal override float GetCost()
        {
            return 0f;
        }

        #endregion
    }
}