using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Actions/Human Actions/HumanAction_Wander")]
	[DisallowMultipleComponent]
	public class HumanAction_Wander : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods
		#endregion
	}
}