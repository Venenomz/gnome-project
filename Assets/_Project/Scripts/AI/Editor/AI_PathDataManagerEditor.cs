using UnityEngine;
using UnityEditor;

namespace GnomeProject
{
    [CustomEditor(typeof(AI_PathDataManager))]
	public class AI_PathDataManagerEditor : Editor
	{
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Generate PathData"))
                (serializedObject.targetObject as AI_PathDataManager).OnEditorBuildPathdata();

            serializedObject.ApplyModifiedProperties();
        }
    }
}