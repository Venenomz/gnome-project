using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Systems/Awareness/ProximitySystem/ProximitySystem")]
	[DisallowMultipleComponent] [RequireComponent(typeof(AI_BaseClass))]
	public class ProximitySystem : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		AI_BaseClass _linkedAI;

        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        private void Start()
        {
            _linkedAI = GetComponent<AI_BaseClass>();
        }

        private void Update()
        {
            for (int i = 0; i < Game.DetectableTargetManager.AllTargets.Count; i++)
            {
                DetectableTarget currentTarget = Game.DetectableTargetManager.AllTargets[i];

                if (currentTarget.gameObject == gameObject)
                    continue;

                if (Vector3.Distance(_linkedAI.EyeLocation, currentTarget.transform.position) <= _linkedAI.ProximityDetectionRange)
                    _linkedAI.ReportInProximity(currentTarget);
            }
        }

        #endregion
    }
}