using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	public class TrackedTarget
    {
		private DetectableTarget _target;

		private Vector3 _rawPosition;

		private float _lastSensedTime = -1f;

		private float _awareness;

		/// <summary>
		/// 0 = not aware (will be culled)
		/// 0-1 = rough idea (no set location)
		/// 1-2 = likely target (has location)
		/// 2 = fully detected
		/// </summary>
		internal float Awareness
        {
			get { return _awareness; }
        }

		internal DetectableTarget DetectableTarget
        {
			get { return _target; }
        }

		internal float LastSensedTime
        {
			get { return _lastSensedTime; }
        }

		internal Vector3 RawPosition
        {
			get { return _rawPosition; }
        }

		internal bool UpdateAwareness(DetectableTarget target, Vector3 position, float awareness, float minAwareness)
        {
			float oldAwareness = awareness;

			if (target.NotNull())
            {
				_target = target;
				_rawPosition = position;
				_lastSensedTime = Time.time;
				_awareness = Mathf.Clamp(Mathf.Max(_awareness, minAwareness) + awareness, 0f, 2f);

				if (oldAwareness < 2f && _awareness >= 2f)
					return true;

				if (oldAwareness < 1f && _awareness >= 1f)
					return true;

				if (oldAwareness <= 0f && _awareness >= 0f)
					return true;
            }

			return false;
        }

		internal bool DecayAwareness(float decayTime, float amount)
        {
			// it's too early to decay
			if ((Time.time - _lastSensedTime) < decayTime)
				return false;

			float oldAwareness = _awareness;

			_awareness -= amount;

			if (oldAwareness >= 2f && _awareness < 2f)
				return true;

			if (oldAwareness >= 1f && _awareness < 1f)
				return true;

			return _awareness <= 0f;
        }
    }


	[AddComponentMenu("Gnome Project/AI/Systems/Awareness")]
	[DisallowMultipleComponent] [RequireComponent(typeof(AI_Human))]
	public class AwarenessSystem : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		[SerializeField, FoldoutGroup("Vision")]
		private AnimationCurve _visionSensitivity;

		[SerializeField, FoldoutGroup("Vision")]
		private float _visionMinimumAwareness = 1f;

		[SerializeField, FoldoutGroup("Vision")]
		private float _visionAwarenessBuildRate = 10f;

		[SerializeField, FoldoutGroup("Hearing")]
		private float _hearingMinimumAwareness = 0f;

		[SerializeField, FoldoutGroup("Hearing")]
		private float _hearingAwarenessBuildRate = 0.5f;

		[SerializeField, FoldoutGroup("Proximity")]
		private float _proximityMinimumAwareness = 0f;

		[SerializeField, FoldoutGroup("Proximity")]
		private float _proximityBuildRate = 1f;

		[SerializeField, FoldoutGroup("Awareness Decay")]
		private float _decayDelay = 0.1f;

		[SerializeField, FoldoutGroup("Awareness Decay")]
		private float _decayRate = 0.1f;

		private Dictionary<GameObject, TrackedTarget> _targets;

		internal Dictionary<GameObject, TrackedTarget> ActiveTargets => _targets;

		private AI_Human _linkedAI;

        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        private void Start()
        {
			if (_linkedAI.IsNull())
				_linkedAI = GetComponent<AI_Human>();

			_targets = new Dictionary<GameObject, TrackedTarget>();
        }

        private void Update()
        {
			List<GameObject> toCleanUp = new List<GameObject>();

			foreach(var go in _targets.Keys)
            {
				if (_targets[go].DecayAwareness(_decayDelay, _decayRate * Time.deltaTime))
                {
					if (_targets[go].Awareness <= 0f)
                    {
						_linkedAI.OnFullyLost();

						toCleanUp.Add(go);
                    }
                }
                else
                {
					if (_targets[go].Awareness >= 1f)
                    {
                        _linkedAI.OnLostDetect(go);
                    }
                    else
                    {
                        _linkedAI.OnLostSuspicion();
                    }
                }
            }

			foreach (var target in toCleanUp)
				_targets.Remove(target);
        }

		private void UpdateAwareness(DetectableTarget target, Vector3 positon, float awareness, float minAwareness)
        {
			if (!_targets.ContainsKey(target.gameObject))
				_targets[target.gameObject] = new TrackedTarget();

			if (_targets[target.gameObject].UpdateAwareness(target, positon, awareness, minAwareness))
            {
                if (_targets[target.gameObject].Awareness >= 2f)
                    _linkedAI.OnFullyDetected(target.gameObject);

                else if (_targets[target.gameObject].Awareness >= 1f)
                    _linkedAI.OnDetected(target.gameObject);

                else if (_targets[target.gameObject].Awareness >= 0f)
                    _linkedAI.OnSuspicious();
            }
        }

		internal void ReportCanSee(DetectableTarget seen)
        {
			Vector3 distToTarget = (seen.transform.position - _linkedAI.transform.position).normalized;

			float product = Vector3.Dot(distToTarget, _linkedAI.EyeDirection);

			float awareness = _visionSensitivity.Evaluate(product) * _visionAwarenessBuildRate * Time.deltaTime;

			UpdateAwareness(seen, seen.transform.position, awareness, _visionMinimumAwareness);
        }

		internal void ReportCanHear(GameObject source, Vector3 location, E_HeardSoundCategory category, float intensity)
        {
			float awareness = intensity * _hearingAwarenessBuildRate * Time.deltaTime;

			UpdateAwareness(null, location, awareness, _hearingMinimumAwareness);
        }

		internal void ReportInProximity(DetectableTarget target)
        {
			float awareness = _proximityBuildRate * Time.deltaTime;

			UpdateAwareness(target, target.transform.position, awareness, _proximityMinimumAwareness);
        }

        #endregion
    }
}