using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Systems/Awareness/Hearing/HearingManager")]
	[DisallowMultipleComponent]
	public class HearingManager : Singleton<HearingManager>
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods
		#endregion
	}

	public enum E_HeardSoundCategory
    {
		FOOTSTEP,
		LOUD_NOISE,
		QUIET_NOISE
    }
}