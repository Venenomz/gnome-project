using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Systems/Awareness/Vision/VisionSystem")]
	[DisallowMultipleComponent] [RequireComponent(typeof(AI_BaseClass))]
	public class VisionSystem : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		private AI_BaseClass _linkedAI;

        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        private void Start()
        {
            _linkedAI = GetComponent<AI_BaseClass>();
        }

        private void Update()
        {
            for (int i = 0; i < Game.DetectableTargetManager.AllTargets.Count; i++)
            {
                DetectableTarget currentTarget = Game.DetectableTargetManager.AllTargets[i];

                // if the current target is ourself, skip
                if (currentTarget.gameObject == gameObject)
                    continue;

                Vector3 vecToTarget = currentTarget.transform.position - _linkedAI.EyeLocation;

                // if the target is out of range, they cannot be seen
                if (vecToTarget.sqrMagnitude > (_linkedAI.VisionConeRange * _linkedAI.VisionConeRange))
                    continue;

                vecToTarget.Normalize();

                // if the target is out of range, they cannot be seen
                if (Vector3.Dot(vecToTarget, _linkedAI.EyeDirection) < _linkedAI.CosVisionConeAngle)
                    continue;

                if (Physics.Raycast(_linkedAI.EyeLocation, vecToTarget, out RaycastHit hit, _linkedAI.VisionConeRange, Game.UIManager.DetectionMask, QueryTriggerInteraction.Collide))
                {
                    if (hit.collider.TryGetComponent<DetectableTarget>(out var targ) == currentTarget)
                        _linkedAI.ReportCanSee(targ);
                }
            }
        }

        #endregion
    }
}