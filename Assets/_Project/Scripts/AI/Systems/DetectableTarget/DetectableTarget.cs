using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Systems/DetectableTarget/DetectableTarget")]
	[DisallowMultipleComponent]
	public class DetectableTarget : MonoBehaviour
	{
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        private void Start()
        {
            if (Game.DetectableTargetManager.NotNull())
                Game.DetectableTargetManager.Register(this);
        }

        private void OnDestroy()
        {
            if (Game.DetectableTargetManager.NotNull())
                Game.DetectableTargetManager.DeRegister(this);
        }

        #endregion
    }
}