using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/Systems/DetectableTarget/DetectableTargetManager")]
	[DisallowMultipleComponent]
	public class DetectableTargetManager : Singleton<DetectableTargetManager>
	{
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]

        internal List<DetectableTarget> AllTargets { get; private set; }

        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        internal void Init()
        {
            AllTargets = new List<DetectableTarget>();
        }

        internal void Register(DetectableTarget target)
        {
            AllTargets.Add(target);
        }

        internal void DeRegister(DetectableTarget target)
        {
            AllTargets.Remove(target);
        }

        #endregion
    }
}