using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/AI/AI_Human")]
	[DisallowMultipleComponent] [RequireComponent(typeof(AwarenessSystem))]
	public class AI_Human : AI_BaseClass
	{
		#region Reference Fields

		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		#endregion
	}
}