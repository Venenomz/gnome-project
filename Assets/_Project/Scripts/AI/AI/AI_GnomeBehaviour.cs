using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/AI_Gnome")]
	[DisallowMultipleComponent]
	public class AI_GnomeBehaviour : AI_BaseClass
	{
		#region Reference Fields

		private GnomeData _gnomeData;

		private const int LIFESPAN = 10;

		// 5 mins
		private const float AGE_UP_TIME = 300f;

		private float _currentAgeTime;


		#endregion

		#region Fields
		#endregion

		#region Properties

		#endregion

		#region Methods

		internal override void Init()
        {
			base.Init();

			ApplyTraitModifiers();

			ResetTimer();
        }

		private void ApplyTraitModifiers()
        {
			if (_gnomeData.Traits.NotNull() && _gnomeData.Traits.Count != 0)
			{
                for (int i = 0; i < _gnomeData.Traits.Count; i++)
                {
					Traits currentTrait = _gnomeData.Traits[i];

					currentTrait.ApplyTraitModifiers(this);
                }
			}
		}

		internal void SetData(GnomeData data)
        {
			_gnomeData = data;
        }

        private void Update()
        {
			HandleAge();
        }

        internal void Die()
        {
			//TODO: some animations and smoke partical effect.

			Game.GnomeGenerator.UpdateCurrentGnomeCounter(true);
			Game.LivingGnomes.Remove(_gnomeData);
			Game.DeadGnomes.Add(_gnomeData);

			_gnomeData.SetDeathTime();

			Destroy(gameObject);
        }

		private void HandleAge()
        {		
			if (_gnomeData.Age >= LIFESPAN)
				Die();

			if (_currentAgeTime >= AGE_UP_TIME)
            {
				_gnomeData.AddAge();
				ResetTimer();
            }

			AgeTimer();
        }

		private void AgeTimer()
        {
			_currentAgeTime += Time.deltaTime;
        }

		private void ResetTimer()
		{
			_currentAgeTime = 0;
		}

		#endregion
	}
}