using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GnomeProject 
{
    public class AI_BaseClass : MonoBehaviour, ICameraTrackable
    {
        #region Variables

        [SerializeField, FoldoutGroup("References Vision Variables")]
        protected float _visionConeAngle = 60f;

        [SerializeField, FoldoutGroup("References Vision Variables")]
        protected float _visionConeRange = 30f;

        [SerializeField, FoldoutGroup("References Vision Variables")]
        protected Color _visionConeColor = new Color(1f, 0f, 0f, 0.25f);

        [SerializeField, FoldoutGroup("References Hearing Variables")]
        protected float _hearingRange = 20f;

        [SerializeField, FoldoutGroup("References Hearing Variables")]
        protected Color _hearingRangeColor = new Color(1f, 1f, 0f, 0.25f);

        [SerializeField, FoldoutGroup("References Proximity Variables")]
        protected float _proximityRangeDetection = 20f;

        [SerializeField, FoldoutGroup("References Proximity Variables")]
        protected Color _proximityRangeColor = new Color(1f, 1f, 1f, 0.25f);

        protected AwarenessSystem _awareness;

        protected AI_UIPrompts _promptUI;

        private bool _isMoving = false;

        private string _movementID = "Move";

        private List<AI_PathDataNode> _currentPath;

        private AI_PathData _pathData;

        private void GetNewPath(Vector3 endPosition)
        {
            if (_currentPath.Count > 0 && _currentPath.NotNull())
                _currentPath.Clear();
                
            _currentPath = Game.AIPathFinder.RequestPath(_movementID, transform.position, endPosition, 
                delegate (AI_PathDataNode current, AI_PathDataNode destination) { return Vector3.Distance(current.WorldPosition, destination.WorldPosition); });
        }

        #endregion

            #region Properties

        internal bool IsMoving
        {
            get { return _isMoving; }
        }

        internal Vector3 EyeLocation
        {
            get { return transform.position; }
        }

        internal Vector3 EyeDirection
        {
            get { return transform.forward; }
        }

        public virtual Vector2 Position
        {
            get { return transform.position; }
        }

        internal float VisionConeAngle => _visionConeAngle;

        internal float VisionConeRange => _visionConeRange;

        internal Color VisionConeColor => _visionConeColor;

        internal float HearingRange => _hearingRange;
        internal Color HearingRangeColor => _hearingRangeColor;

        internal float ProximityDetectionRange => _proximityRangeDetection;
        internal Color ProximityDetectionColor => _proximityRangeColor;

        internal float CosVisionConeAngle { get; private set; } = 0f;

        #endregion

        #region Methods

        // Start is called before the first frame update
        internal virtual void Init()
        {
            CosVisionConeAngle = Mathf.Cos(VisionConeAngle * Mathf.Deg2Rad);

            _awareness = GetComponent<AwarenessSystem>();

            if (_promptUI.IsNull())
            {
                var loadPromptUI = Resources.Load<AI_UIPrompts>("AI/UIPrefab");

                if (loadPromptUI.NotNull())
                    _promptUI = loadPromptUI;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        internal virtual void ReportCanSee(DetectableTarget seen)
        {
            _awareness.ReportCanSee(seen);
        }

        internal virtual void ReportCanHear(GameObject source, Vector3 location, E_HeardSoundCategory category, float intensity)
        {
            _awareness.ReportCanHear(source, location, category, intensity);
        }

        internal virtual void ReportInProximity(DetectableTarget target)
        {
            _awareness.ReportInProximity(target);
        }

        internal virtual void OnSuspicious()
        {
            _promptUI.SetText("I heard something!");
        }

        internal virtual void OnDetected(GameObject target)
        {
            _promptUI.SetText("Woah, what is that?! " + target.gameObject.name);
        }

        internal virtual void OnFullyDetected(GameObject target)
        {
            _promptUI.SetText("Charge! " + target.gameObject.name);
        }

        internal virtual void OnLostDetect(GameObject target)
        {
            _promptUI.SetText("Where did you go? " + target.gameObject.name);
        }

        internal virtual void OnLostSuspicion()
        {
            _promptUI.SetText("Maybe I was just seeing things.. weird.");
        }

        internal virtual void OnFullyLost()
        {
            _promptUI.SetText("Must be nothing then.");
        }

        internal void StartMove(Vector3 location)
        {
            GetNewPath(location);

            if (_currentPath.IsNull() || _currentPath.Count == 0)
                return;

            StartCoroutine(PerformMovement());
        }

        private IEnumerator PerformMovement()
        {
            int currentNodeIndex = 0;

            while(currentNodeIndex <= _currentPath.Count)
            {
                if (ArrivedAtCurrentNode(_currentPath[currentNodeIndex]))
                    currentNodeIndex++;

                Vector3 moveTowards = Vector3.MoveTowards(transform.position, _currentPath[currentNodeIndex].WorldPosition, 2f * Time.deltaTime);

                transform.Translate(moveTowards);

                yield return null;
            }
        }

        private bool ArrivedAtCurrentNode(AI_PathDataNode node)
        {
            Vector3 distanceToNode = node.WorldPosition - transform.position;

            if (distanceToNode.magnitude < 0.1f)
                return true;

            return false;
        }

        #endregion
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(AI_BaseClass))]
    public class AI_Editor : Editor
    {
        private void OnSceneGUI()
        {
            AI_BaseClass ai = target as AI_BaseClass;

            Handles.color = ai.ProximityDetectionColor;
            Handles.DrawSolidDisc(ai.transform.position, Vector3.up, ai.ProximityDetectionRange);

            // draw the hearing range
            Handles.color = ai.HearingRangeColor;
            Handles.DrawSolidDisc(ai.transform.position, Vector3.up, ai.HearingRange);

            // work out the start point of the vision cone
            Vector3 startPoint = Mathf.Cos(-ai.VisionConeAngle * Mathf.Deg2Rad) * ai.transform.forward +
                                 Mathf.Sin(-ai.VisionConeAngle * Mathf.Deg2Rad) * ai.transform.right;

            // draw the vision cone
            Handles.color = ai.VisionConeColor;
            Handles.DrawSolidArc(ai.transform.position, Vector3.up, startPoint, ai.VisionConeAngle * 2f, ai.VisionConeRange);
        }
    }

#endif
}
