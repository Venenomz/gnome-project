using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/UI/AI_UIPrompts")]
	[DisallowMultipleComponent] [RequireComponent(typeof(GUIStateMember))]
	public class AI_UIPrompts : MonoBehaviour
	{
		#region Reference Fields
		[SerializeField, FoldoutGroup("References")]
		private TMP_Text _promptText;

		private GUIStateMember _stateMember;

		private float _currentTime = 0f;
		private float _maxActiveTime = 4f;

		private bool _active = false;

        #endregion

        #region Fields
        #endregion

        #region Properties

        #endregion

        #region Methods

        private void Update()
        {
			if (_active)
				DoTimer();
        }

        internal void Init()
        {
			_stateMember = GetComponent<GUIStateMember>();
        }

		internal void SetText(string toSet)
        {
			ResetTimer();

			if (_promptText.NotNull())
				_promptText.text = toSet;
        }

		private void DoTimer()
        {
			_currentTime -= Time.deltaTime;

			if (_currentTime <= 0)
            {
				_stateMember.SetActive(false);
				_active = false;
            }
        }

		private void ResetTimer()
        {
			_currentTime = _maxActiveTime;

			_stateMember.SetActive(true);

			_active = true;
		}

		#endregion
	}
}