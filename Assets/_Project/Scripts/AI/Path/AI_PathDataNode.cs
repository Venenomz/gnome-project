using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;

namespace GnomeProject
{
    [System.Flags]
    public enum E_PathDataNodeAttributes : byte
    {
        NONE = 0x00,
        WALKABLE = 0x01,
        IS_BOUNDARY = 0x80
    }

    [System.Flags]
    public enum E_NeighbourFlags : byte
    {
        NONE = 0x00,
        NORTH = 0x01,
        NORTH_EAST = 0x02,
        EAST = 0x04,
        SOUTH_EAST = 0x08,
        SOUTH = 0x10,
        SOUTH_WEST = 0x20,
        WEST = 0x40,
        NORTH_WEST = 0x80
    }

    public static class GridHelpers
    {
        public static Vector3Int s_stepNorth = new Vector3Int(0, 1, 0);
        public static Vector3Int s_stepNorthEast = new Vector3Int(1, 1, 0);
        public static Vector3Int s_stepEast = new Vector3Int(1, 0, 0);
        public static Vector3Int s_stepSouthEast = new Vector3Int(1, -1, 0);
        public static Vector3Int s_stepSouth = new Vector3Int(0, -1, 0);
        public static Vector3Int s_stepSouthWest = new Vector3Int(-1, -1, 0);
        public static Vector3Int s_stepWest = new Vector3Int(-1, 0, 0);
        public static Vector3Int s_stepNorthWest = new Vector3Int(-1, 1, 0);
    }

    public enum E_Resolution
    {
        NODESIZE_1x1 = 1,
        NODESIZE_2x2 = 2,
        NODESIZE_4x4 = 4,
        NODESIZE_8x8 = 8,
        NODESIZE_16x16 = 16
    }

    public class AI_PathDataNode
    {
        private Vector3 _worldPos;
        private Vector3 _gridPos;

        private E_PathDataNodeAttributes _attributes;
        private E_NeighbourFlags _neighbourFlags;

        private int _areaID;

        private int _uniqueID;

        public E_PathDataNodeAttributes Attributes
        {
            get
            {
                return _attributes;
            }
            set
            {
                _attributes = value;
            }
        }

        public E_NeighbourFlags NeighbourFlags
        {
            get
            {
                return _neighbourFlags;
            }
            set
            {
                _neighbourFlags = value;
            }
        }

        public Vector3 WorldPosition
        {
            get
            {
                return _worldPos;
            }
            set
            {
                _worldPos = value;
            }
        }

        public Vector3 GridPosition
        {
            get
            { 
                return _gridPos; 
            }
            set 
            { 
                _gridPos = value; 
            }
        }

        public int AreaID 
        { 
            get 
            { 
                return _areaID; 
            }
            set
            {
                _areaID = value;
            }
        }

        public int UniqueID 
        { 
            get 
            { 
                return _uniqueID; 
            } 
            set { _uniqueID = value; }
        }

        public bool IsBoundary => _attributes.HasFlag(E_PathDataNodeAttributes.IS_BOUNDARY);
        public bool IsWalkable => _attributes.HasFlag(E_PathDataNodeAttributes.WALKABLE);

        public bool HasNeighbour_N => _neighbourFlags.HasFlag(E_NeighbourFlags.NORTH);
        public bool HasNeighbour_NE => _neighbourFlags.HasFlag(E_NeighbourFlags.NORTH_EAST);
        public bool HasNeighbour_E => _neighbourFlags.HasFlag(E_NeighbourFlags.EAST);
        public bool HasNeighbour_SE => _neighbourFlags.HasFlag(E_NeighbourFlags.SOUTH_EAST);
        public bool HasNeighbour_S => _neighbourFlags.HasFlag(E_NeighbourFlags.SOUTH);
        public bool HasNeighbour_SW => _neighbourFlags.HasFlag(E_NeighbourFlags.SOUTH_WEST);
        public bool HasNeighbour_W => _neighbourFlags.HasFlag(E_NeighbourFlags.WEST);
        public bool HasNeighbour_NW => _neighbourFlags.HasFlag(E_NeighbourFlags.NORTH_WEST);
    }

    [System.Serializable]
    public class AI_PathData : ScriptableObject, ISerializationCallbackReceiver
    {
        private string _uniqueID;

        private Vector2Int _dimensions;

        private Vector3 _cellSize;

        private E_Resolution _resolution = E_Resolution.NODESIZE_1x1;

        [SerializeField]
        E_PathDataNodeAttributes[] _attributes;

        [SerializeField]
        E_NeighbourFlags[] _neighbourFlags;

        [SerializeField]
        float[] _heights;

        [SerializeField]
        int[] _areaIDs;

        public Vector2Int Dimensions
        {
            get { return _dimensions; }
        }

        public E_Resolution Resolution
        {
            get { return _resolution; }
        }

        [System.NonSerialized]
        private AI_PathDataNode[] _nodes;

        public AI_PathDataNode[] Nodes
        {
            get { return _nodes; }
        }

        public void Init(string id, E_Resolution resolution, Vector2Int dimensions, Vector3 cellSize)
        {
            _uniqueID = id;
            _resolution = resolution;
            _dimensions = dimensions;
            _cellSize = cellSize;

            _nodes = new AI_PathDataNode[_dimensions.x * _dimensions.y];

            for (int i = 0; i < _nodes.Length; i++)
                _nodes[i] = new AI_PathDataNode();
        }

        public void InitNode(int row, int column, Vector3 worldPosition, E_PathDataNodeAttributes attributes)
        {
            int nodeIndex = column + (row * _dimensions.x);

            _nodes[nodeIndex].UniqueID = nodeIndex;
            _nodes[nodeIndex].GridPosition = new Vector3Int(column, row, 0);
            _nodes[nodeIndex].WorldPosition = worldPosition;
            _nodes[nodeIndex].Attributes = attributes;
            _nodes[nodeIndex].AreaID = -1;
        }

        public void OnAfterDeserialize()
        {
            _nodes = new AI_PathDataNode[_attributes.Length];

            for (int i = 0; i < _attributes.Length; i++)
            {
                _nodes[i] = new AI_PathDataNode();

                _nodes[i].UniqueID = i;
                _nodes[i].Attributes = _attributes[i];
                _nodes[i].AreaID = _areaIDs[i];

                int x = i % _dimensions.x;
                int y = (i - x) / _dimensions.x;

                _nodes[i].GridPosition = new Vector3Int(x, y, 0);

                _nodes[i].WorldPosition = new Vector3((y + 0.5f) * _cellSize.x, _heights[i], (x + 0.5f) * _cellSize.z);
            }
        }

        public void OnBeforeSerialize()
        {
            if (_nodes.NotNullOrEmpty())
            {
                _attributes = null;
                _neighbourFlags = null;
                _heights = null;
                _areaIDs = null;

                return;
            }

            // extract data from the nodes
            _attributes = new E_PathDataNodeAttributes[_nodes.Length];
            _neighbourFlags = new E_NeighbourFlags[_nodes.Length];
            _heights = new float[_nodes.Length];
            _areaIDs = new int[_nodes.Length];

            for (int i = 0; i < _attributes.Length; ++i)
            {
                _attributes[i] = _nodes[i].Attributes;
                _neighbourFlags[i] = _nodes[i].NeighbourFlags;
                _heights[i] = _nodes[i].WorldPosition.y;
                _areaIDs[i] = _nodes[i].AreaID;
            }
        }

        public AI_PathDataNode GetNode(int id)
        {
            if (id < 0 || id >= _nodes.Length)
                return null;

            int column = id % _dimensions.x;

            int row = (id - column) / _dimensions.x;

            return GetNode(row, column);
        }

        public AI_PathDataNode GetNode(Vector3 worldPosition)
        {
            int row = Mathf.FloorToInt((worldPosition.x / _cellSize.x) - 0.5f);
            int column = Mathf.FloorToInt((worldPosition.z / _cellSize.z) - 0.5f);

            return GetNode(row, column);
        }

        public AI_PathDataNode GetNode(Vector3Int gridPosition)
        {
            return GetNode(gridPosition.y, gridPosition.x);
        }

        public AI_PathDataNode GetNode(int row, int column)
        {
            if (row < 0 || column < 0 || row >= _dimensions.y || column >= _dimensions.x)
                return null;

            return _nodes[column + (row * _dimensions.x)];
        }
    }
}
