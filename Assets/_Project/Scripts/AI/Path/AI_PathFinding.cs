using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;

namespace GnomeProject
{
    public class AI_PathFinding : Singleton<AI_PathFinding>
    {
        private string _pathDataUID;
        private Transform _startTransform;
        private Transform _endTransform;

        // Start is called before the first frame update
        void Start()
        {

        }

        internal void Init()
        {
            
        }

        internal List<AI_PathDataNode> RequestPath(string pathUID, Vector3 startPosition, Vector3 endPosition, System.Func<AI_PathDataNode, AI_PathDataNode, float> calculateCost)
        {
            System.Diagnostics.Stopwatch pathfindingTimer = new System.Diagnostics.Stopwatch();

            var pathData = Game.PathDataManager.GetPathData(pathUID);

            if (pathData.IsNull())
                return null;

            AI_PathDataNode startNode = pathData.GetNode(startPosition);

            if (startNode.IsNull())
                return null;

            AI_PathDataNode endNode = pathData.GetNode(endPosition);

            if (endNode.IsNull())
                return null;

            if (startNode.AreaID != endNode.AreaID || startNode.AreaID < 1 || endNode.AreaID < 1)
                return null;

            pathfindingTimer.Start();

            // Generate context for this path
            PathFindingContext context = new PathFindingContext(pathData.Nodes.Length);

            context.OpenNode(startNode, 0f, calculateCost(startNode, endNode), -1);

            // while we have nodes to explore: 
            while (context.OpenListNotEmpty)
            {
                AI_PathDataNode bestNode = context.GetBestNode();

                // reached the end of the path
                if (bestNode == endNode)
                {
                    List<AI_PathDataNode> newPath = new List<AI_PathDataNode>();

                    while (bestNode.NotNull())
                    {
                        newPath.Insert(0, bestNode);

                        bestNode = pathData.GetNode(context.GetParentID(bestNode));
                    }

                    if (Utils.UnityEditor())
                        Debug.Log("Path found in " + context.NumIterations + " iterations.");

                    pathfindingTimer.Stop();

                    Debug.Log(pathfindingTimer.ElapsedMilliseconds);

                    return newPath;
                }

                context.MoveToClosed(bestNode);

                for (int neighbourIndex = 0; neighbourIndex < NeightboursToCheck.Length; neighbourIndex++)
                {
                    // No neighbour exists
                    if (!bestNode.NeighbourFlags.HasFlag(NeightboursToCheck[neighbourIndex]))
                        continue;

                    AI_PathDataNode neighbourNode = pathData.GetNode(bestNode.GridPosition + NeighbourOffsets[neighbourIndex]);

                    // The node is closed
                    if (context.ClosedNode(neighbourNode))
                        continue;

                    if (context.NodeOpen(neighbourNode))
                    {
                        float gCost = context.GetGCost(bestNode) + calculateCost(bestNode, neighbourNode);

                        // a more desirable node as appeared
                        if (gCost < context.GetGCost(neighbourNode))
                            context.UpdateOpenNode(neighbourNode, gCost, bestNode.UniqueID);
                    }
                    else
                    {
                        // Calculate g cost to reach neighbour
                        float gCost = context.GetGCost(bestNode) + calculateCost(bestNode, neighbourNode);

                        context.OpenNode(neighbourNode, gCost, calculateCost(neighbourNode, endNode), bestNode.UniqueID);
                    }
                }
            }

            if (Utils.UnityEditor())
                Debug.LogError("No Path Could Be Found.");

            pathfindingTimer.Stop();

            return null;
        }

        E_NeighbourFlags[] NeightboursToCheck = new E_NeighbourFlags[]
        {
            E_NeighbourFlags.NORTH,
            E_NeighbourFlags.NORTH_EAST,
            E_NeighbourFlags.EAST,
            E_NeighbourFlags.SOUTH_EAST,
            E_NeighbourFlags.SOUTH,
            E_NeighbourFlags.SOUTH_WEST,
            E_NeighbourFlags.WEST,
            E_NeighbourFlags.NORTH_WEST
        };

        Vector3Int[] NeighbourOffsets = new Vector3Int[]
        {
            GridHelpers.s_stepNorth, 
            GridHelpers.s_stepNorthEast,
            GridHelpers.s_stepEast,
            GridHelpers.s_stepSouthEast,
            GridHelpers.s_stepSouth,
            GridHelpers.s_stepSouthWest,
            GridHelpers.s_stepWest,
            GridHelpers.s_stepNorthWest
        };
    }

    public class PathFindingContext
    {
        [System.Flags]
        enum E_NodeStatus
        {
            NONE = 0x00,
            OPEN = 0x01,
            CLOSED = 0x02
        }

        E_NodeStatus[] _statuses;

        float[] _gCosts;
        float[] _hCosts;

        int[] _parentIDs;

        public bool OpenListNotEmpty
        {
            get { return _openNodeList.Count > 0; }
        }

        public int NumIterations { get; private set; } = 0;

        private List<AI_PathDataNode> _openNodeList = new List<AI_PathDataNode>();

        public PathFindingContext(int numNodes)
        {
            _statuses = new E_NodeStatus[numNodes];
            _gCosts = new float[numNodes];
            _hCosts = new float[numNodes];

            _parentIDs = new int[numNodes];

            for (int i = 0; i < numNodes; i++)
            {
                _gCosts[i] = _hCosts[i] = float.MaxValue;
                _parentIDs[i] = -1;
            }
        }

        public void OpenNode(AI_PathDataNode node, float gCost, float hCost, int parentID)
        {
            _openNodeList.Add(node);

            _statuses[node.UniqueID] = E_NodeStatus.OPEN;
            _gCosts[node.UniqueID] = gCost;
            _hCosts[node.UniqueID] = hCost;
            _parentIDs[node.UniqueID] = parentID;
        }

        public void UpdateOpenNode(AI_PathDataNode node, float gCost, int parentID)
        {
            _gCosts[node.UniqueID] = gCost;
            _parentIDs[node.UniqueID] = parentID;
        }

        public void MoveToClosed(AI_PathDataNode node)
        {
            _openNodeList.Remove(node);
            _statuses[node.UniqueID] = E_NodeStatus.CLOSED;
        }

        public float GetGCost(AI_PathDataNode node)
        {
            return _gCosts[node.UniqueID];
        }

        public float GetFCost(AI_PathDataNode node)
        {
            return _gCosts[node.UniqueID] + _hCosts[node.UniqueID];
        }

        public int GetParentID(AI_PathDataNode node)
        {
            return _parentIDs[node.UniqueID];
        }

        public bool NodeOpen(AI_PathDataNode node)
        {
            return _statuses[node.UniqueID] == E_NodeStatus.OPEN;
        }

        public bool ClosedNode(AI_PathDataNode node)
        {
            return _statuses[node.UniqueID] == E_NodeStatus.CLOSED;
        }

        public AI_PathDataNode GetBestNode()
        {
            ++NumIterations;

            float bestFCost = float.MaxValue;

            AI_PathDataNode bestNode = null;

            for (int i = 0; i < _openNodeList.Count; i++)
            {
                float cost = GetFCost(_openNodeList[i]);

                if (cost < bestFCost)
                {
                    bestFCost = cost;
                    bestNode = _openNodeList[i];
                }
            }

            return bestNode;
        }
    }
}
