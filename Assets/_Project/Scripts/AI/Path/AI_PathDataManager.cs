using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using LobsterDog;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/AI/AI_PathDataManager")]
	[DisallowMultipleComponent]
	public class AI_PathDataManager : Singleton<AI_PathDataManager>
	{
		#region Reference Fields
		[SerializeField, FoldoutGroup("Nodes")] private List<AI_PathDataSet> _pathDataSet;

        [SerializeField, FoldoutGroup("Terrain Data")] Terrain _sourceTerrain;

        [SerializeField] Texture2D _sourceBiomeMap;

        [SerializeField] Texture2D _sourceSlopeMap;

        [SerializeField, Tooltip("This variable dictates how large the grid of nodes will be.")]
        private Vector2Int _gridSize;

		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		internal void Init()
        {

        }

		internal AI_PathData GetPathData(string id)
        {
            for (int i = 0; i < _pathDataSet.Count; i++)
            {
				AI_PathDataSet data = _pathDataSet[i];

				if (data._uniqueID == id)
					return data._pathData;
			}

			return null;
        }

        #region Debug Methods

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
            Vector3 cameraLocation = Game.GameCamera.CameraPosition;

            for (int x = 0; x < _pathDataSet.Count; ++x)
            {
                var pathdataSet = _pathDataSet[x];

                // pathdata not yet built
                if (pathdataSet._pathData == null)
                    continue;

                for (int y = 0; y < pathdataSet._pathData.Nodes.Length; ++y)
                {
                    var node = pathdataSet._pathData.Nodes[y];

                    if (node == null)
                        continue;

                    if ((node.WorldPosition - cameraLocation).sqrMagnitude > (50 * 50))
                        continue;

                    DrawDebug(pathdataSet, node);
                }
            }
        }

        static Color[] AreaBank = new Color[] { Color.red, Color.black, Color.blue, Color.green, Color.cyan, Color.yellow, Color.magenta, Color.white };

        void DrawDebug(AI_PathDataSet pathdataSet, AI_PathDataNode node)
        {
            if (node.IsBoundary)
                return;

            // draw areas?
            if (pathdataSet.Debug_ShowAreas)
            {
                if (node.AreaID < 1)
                    return;

                Gizmos.color = AreaBank[node.AreaID % AreaBank.Length];
                Gizmos.DrawLine(node.WorldPosition, node.WorldPosition + Vector3.up);

                return;
            }

            // draw the nodes?
            if (pathdataSet.Debug_ShowNodes)
            {
                if (node.IsWalkable)
                    Gizmos.color = Color.green;
                else
                    Gizmos.color = Color.red;

                Gizmos.DrawLine(node.WorldPosition, node.WorldPosition + Vector3.up);
            }

            // draw the edges
            if (pathdataSet.Debug_ShowEdges)
            {
                var pathdata = pathdataSet._pathData;

                Gizmos.color = Color.white;
                if (node.HasNeighbour_N)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepNorth).WorldPosition);

                if (node.HasNeighbour_NE)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepNorthEast).WorldPosition);

                if (node.HasNeighbour_E)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepEast).WorldPosition);

                if (node.HasNeighbour_SE)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepSouthEast).WorldPosition);

                if (node.HasNeighbour_S)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepSouth).WorldPosition);

                if (node.HasNeighbour_SW)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepSouthWest).WorldPosition);

                if (node.HasNeighbour_W)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepWest).WorldPosition);

                if (node.HasNeighbour_NW)
                    Gizmos.DrawLine(node.WorldPosition + Vector3.up, pathdata.GetNode(node.GridPosition + GridHelpers.s_stepNorthWest).WorldPosition);
            }
        }

        public void OnEditorBuildPathdata()
        {
            InternalBuildPathData();
        }

#endif
        #endregion

        void InternalBuildPathData()
        {
            for (int i = 0; i < _pathDataSet.Count; ++i)
                InternalBuildPathData(_pathDataSet[i], i);
        }

        void InternalBuildPathData(AI_PathDataSet pathdataSet, int pathdataIndex)
        {
            // allocate the pathdata
            var pathdata = ScriptableObject.CreateInstance<AI_PathData>();

#if UNITY_EDITOR
            Undo.RegisterCreatedObjectUndo(pathdata, "Create PathData");

            string fileName = SceneManager.GetActiveScene().name + "_Pathdata_" + pathdataIndex;
            string assetPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(SceneManager.GetActiveScene().path), fileName + ".asset");
            System.IO.File.Delete(assetPath);

            AssetDatabase.CreateAsset(pathdata, assetPath);
#endif      

            pathdataSet._pathData = pathdata;

            // determine the pathdata size, allowing for scale
            var pathdataSize = (_sourceTerrain.terrainData.heightmapResolution - 1) / pathdataSet._resolution.ToInt();

            // determine the scaled node size
            var nodeSize = _sourceTerrain.terrainData.heightmapScale;
            nodeSize.x *= pathdataSet._resolution.ToInt();
            nodeSize.z *= pathdataSet._resolution.ToInt();

            pathdata.Init(pathdataSet._uniqueID, pathdataSet._resolution, new Vector2Int(pathdataSize, pathdataSize), nodeSize);

            InternalBuildPathData(pathdata, pathdataSet, pathdataSize, nodeSize);

#if UNITY_EDITOR
            // flag the asset as dirty and save it
            EditorUtility.SetDirty(pathdata);
            AssetDatabase.SaveAssets();
#endif          
        }

        void InternalBuildPathData(AI_PathData pathdata, AI_PathDataSet configuration, int pathdataSize, Vector3 nodeSize)
        {
            // extract key data
            var heightMap = _sourceTerrain.terrainData.GetHeights(0, 0, _sourceTerrain.terrainData.heightmapResolution, _sourceTerrain.terrainData.heightmapResolution);

            var cosSlopeLimit = Mathf.Cos(configuration._slopeLimit * Mathf.Deg2Rad);

            // build the nodes
            for (int row = 0; row < pathdataSize; ++row)
            {
                for (int column = 0; column < pathdataSize; ++column)
                {
                    // retrieve the height
                    float height = SampleHeightMap(heightMap, configuration._resolution, row, column);

                    // generate world pos
                    Vector3 worldPos = new Vector3((row + 0.5f) * nodeSize.z, height * nodeSize.y, (column + 0.5f) * nodeSize.x);

                    // determine the slope
                    float cosSlope = SampleSlopeMap(configuration._resolution, row, column);

                    // build the attributes
                    E_PathDataNodeAttributes attributes = E_PathDataNodeAttributes.NONE;

                      if (cosSlope >= cosSlopeLimit)
                        attributes |= E_PathDataNodeAttributes.WALKABLE;

                    if (row == 0 || column == 0 || row == (pathdataSize - 1) || column == (pathdataSize - 1))
                        attributes = E_PathDataNodeAttributes.IS_BOUNDARY;

                    // initialise the node
                    pathdata.InitNode(row, column, worldPos, attributes);
                }
            }

            // build up the neighbours
            for (int row = 0; row < pathdataSize; ++row)
            {
                for (int column = 0; column < pathdataSize; ++column)
                {
                    var node = pathdata.GetNode(row, column);

                    // boundary nodes do not connect
                    if (node.IsBoundary)
                        continue;

                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.NORTH, GridHelpers.s_stepNorth);
                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.NORTH_EAST, GridHelpers.s_stepNorthEast);
                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.EAST, GridHelpers.s_stepEast);
                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.SOUTH_EAST, GridHelpers.s_stepSouthEast);
                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.SOUTH, GridHelpers.s_stepSouth);
                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.SOUTH_WEST, GridHelpers.s_stepSouthWest);
                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.WEST, GridHelpers.s_stepWest);
                    UpdateNeighbourFlags(node, pathdata, configuration, E_NeighbourFlags.NORTH_WEST, GridHelpers.s_stepNorthWest);
                }
            }

            BuildAreaIds(pathdata);
        }

        void UpdateNeighbourFlags(AI_PathDataNode currentNode, AI_PathData pathdata, AI_PathDataSet configuration, E_NeighbourFlags directionFlag, Vector3Int offset)
        {
            var neighbour = pathdata.GetNode(currentNode.GridPosition + offset);

            // no neighbour or it's a boundary so do nothing
            if (neighbour == null || neighbour.IsBoundary)
                return;

            // link only if both the same
            if ((neighbour.IsWalkable && currentNode.IsWalkable))
                currentNode.NeighbourFlags |= directionFlag;

            // TODO: Make config useful
        }

        void BuildAreaIds(AI_PathData pathdata)
        {
            bool[] flaggedForProcessing = new bool[pathdata.Dimensions.x * pathdata.Dimensions.y];

            int currentAreaID = 1;

            int minAreaSize = int.MaxValue;
            int maxAreaSize = int.MinValue;

            for (int index = 0; index < pathdata.Nodes.Length; ++index)
            {
                // skip if already visited
                if (pathdata.Nodes[index].AreaID >= 0)
                    continue;

                // node has no connections - mark as isolated
                if (pathdata.Nodes[index].NeighbourFlags == E_NeighbourFlags.NONE)
                {
                    pathdata.Nodes[index].AreaID = 0;

                    continue;
                }

                int areaSize = FloodFillArea(pathdata.Nodes[index], pathdata, currentAreaID, flaggedForProcessing);

                ++currentAreaID;

                minAreaSize = Mathf.Min(areaSize, minAreaSize);
                maxAreaSize = Mathf.Max(areaSize, maxAreaSize);
            }

            if (Utils.UnityEditor())
                Debug.Log("Min: " + minAreaSize + ", Max: " + maxAreaSize + ", Total: " + currentAreaID);
        }

        int FloodFillArea(AI_PathDataNode seedNode, AI_PathData pathdata, int areaID, bool[] flaggedForProcessing)
        {
            Queue<Vector3Int> nodesToProcess = new Queue<Vector3Int>();

            nodesToProcess.Enqueue(new Vector3Int(seedNode.GridPosition.x.FloatToInt(), seedNode.GridPosition.y.FloatToInt(), seedNode.GridPosition.z.FloatToInt()));

            int areaSize = 0;

            // while we have things to process
            while (nodesToProcess.Count > 0)
            {
                AI_PathDataNode node = pathdata.GetNode(nodesToProcess.Dequeue());

                if (node.AreaID >= 0)
                    continue;

                // tag the node
                node.AreaID = areaID;
                ++areaSize;

                // add in any neighbours
                if (node.HasNeighbour_N)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepNorth, nodesToProcess, flaggedForProcessing);

                if (node.HasNeighbour_NE)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepNorthEast, nodesToProcess, flaggedForProcessing);

                if (node.HasNeighbour_E)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepEast, nodesToProcess, flaggedForProcessing);

                if (node.HasNeighbour_SE)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepSouthEast, nodesToProcess, flaggedForProcessing);

                if (node.HasNeighbour_S)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepSouth, nodesToProcess, flaggedForProcessing);

                if (node.HasNeighbour_SW)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepSouthWest, nodesToProcess, flaggedForProcessing);

                if (node.HasNeighbour_W)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepWest, nodesToProcess, flaggedForProcessing);

                if (node.HasNeighbour_NW)
                    FloodFillArea_Helper(node, pathdata, GridHelpers.s_stepNorthWest, nodesToProcess, flaggedForProcessing);
            }

            return areaSize;
        }

        void FloodFillArea_Helper(AI_PathDataNode currentNode, AI_PathData pathdata, Vector3Int offset, Queue<Vector3Int> nodesToProcess, bool[] flaggedForProcessing)
        {
            AI_PathDataNode neighbour = pathdata.GetNode(currentNode.GridPosition + offset);

            int neighbourIndex = neighbour.GridPosition.x.FloatToInt() + neighbour.GridPosition.y.FloatToInt() * pathdata.Dimensions.x;

            // if already flagged then ignore
            if (flaggedForProcessing[neighbourIndex])
                return;

            // if already processed then ignore
            if (neighbour.AreaID < 0)
            {
                nodesToProcess.Enqueue(neighbour.GridPosition.ToVector3Int());
                flaggedForProcessing[neighbourIndex] = true;
            }
        }

        #endregion

        float SampleHeightMap(float[,] heightMap, E_Resolution resolution, int row, int column)
        {
            float heightSum = 0f;
            int numSamples = 0;

            int range = resolution.ToInt() + 1;

            // retrieve the key heightmap parameters
            int heightMapSize = heightMap.GetLength(0);
            int startingRow = row * resolution.ToInt();
            int startingCol = column * resolution.ToInt();

            // sum the height values
            for (int workingRow = startingRow; workingRow < (startingRow + range) && workingRow < heightMapSize; ++workingRow)
            {
                for (int workingCol = startingCol; workingCol < (startingCol + range) && workingCol < heightMapSize; ++workingCol)
                {
                    heightSum += heightMap[workingCol, workingRow];
                    ++numSamples;
                }
            }

            return numSamples == 0 ? 0f : (heightSum / numSamples);
        }

        float SampleSlopeMap(E_Resolution resolution, int row, int column)
        {
            float minSlope = 1f;

            int range = resolution.ToInt() + 1;

            // retrieve the key parameters
            int startingRow = row * resolution.ToInt();
            int startingCol = column * resolution.ToInt();

            // sum the slope values
            for (int workingRow = startingRow; workingRow < (startingRow + range) && workingRow < _sourceSlopeMap.height; ++workingRow)
            {
                for (int workingCol = startingCol; workingCol < (startingCol + range) && workingCol < _sourceSlopeMap.width; ++workingCol)
                    minSlope = Mathf.Min(_sourceSlopeMap.GetPixel(workingRow, workingCol).r, minSlope);
            }

            return minSlope;
        }
    }


    [System.Serializable]
	public class AI_PathDataSet
    {
		public string _uniqueID;

		public E_Resolution _resolution = E_Resolution.NODESIZE_1x1;

		public float _slopeLimit = 45f;

		public AI_PathData _pathData;

		public bool Debug_ShowAreas = false;

		public bool Debug_ShowNodes = false;

		public bool Debug_ShowEdges = false;
    }
}