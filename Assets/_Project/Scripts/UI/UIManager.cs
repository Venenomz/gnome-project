using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
    public class UIManager : Singleton<UIManager>
    {
        #region Variables

        [SerializeField, FoldoutGroup("States")][ReadOnly]
        private UIState[] _uiStates;

        private UIState _currentState = null;

        private UIState _previousState = null;

        [System.NonSerialized]
        private Dictionary<string, UIState> _stateLookup = new Dictionary<string, UIState>();

        #endregion

        #region Properties

        internal int InteractionLayer
        {
            get { return LayerMask.GetMask("Interaction"); }
        }

        internal int DetectionMask
        {
            get { return LayerMask.GetMask("Detection"); }
        }

        #endregion

        #region Methods
        internal void Init()
        {
            _uiStates = GetComponentsInChildren<UIState>();

            if (_uiStates.NotNullOrEmpty())
            {
                for (int i = 0; i < _uiStates.Length; i++)
                {
                    UIState state = _uiStates[i];

                    state.Init();

                    _stateLookup[state.StateID] = state;
                }

                SwitchMenuState(_uiStates[0].StateID);
            }
        }

        /// <summary>
        /// Switches the current menu state to the one with the given ID.
        /// </summary>
        /// <param name="id"></param>
        internal void SwitchMenuState(string id)
        {
            UIState goingTo = _stateLookup[id];

            if (goingTo.NotNull())
            {
                _previousState = _currentState;

                if (_previousState.NotNull())
                    _previousState.OnSetDeactive();

                _currentState = goingTo;

                _currentState.OnSetActive();
            }
        }

        public UIState GetStateById(string ID)
        {
            UIState toReturn = _stateLookup[ID];

            if (toReturn == null)
                return null;

            return toReturn;
        }

        #endregion
    }
}
