using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/UI/UIState")]
	[DisallowMultipleComponent] [RequireComponent(typeof(GUIStateMember))]
	public class UIState : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		[SerializeField, Tooltip("This has to be set to a value otherwise Ui states will not work.")]
		protected string _stateId;

		protected GUIStateMember _guiStateMember = null;

		[SerializeField, ReadOnly]
		protected bool _inFocus = false;
		
		#endregion

		#region Fields
		#endregion

		#region Properties

		internal string StateID
        {
			get { return _stateId; }
        }

		#endregion

		#region Methods

		internal virtual void Init()
        {
			_guiStateMember = GetComponent<GUIStateMember>();
        }

		internal virtual void OnSetActive()
        {
			if (_guiStateMember.NotNull())
				_guiStateMember.SetActive(true);

			_inFocus = true;

			if (Utils.UnityEditor())
				Debug.Log("UI State " + _stateId + " => Setting Active");
        }

		internal virtual void OnSetDeactive()
        {
			if (_guiStateMember.NotNull())
				_guiStateMember.SetActive(false);

			_inFocus = false;

			if (Utils.UnityEditor())
				Debug.Log("UI State " + _stateId + " => Setting Deactive");
		}

		internal virtual void ClosePanel()
        {
			OnSetDeactive();
        }

		#endregion
	}
}