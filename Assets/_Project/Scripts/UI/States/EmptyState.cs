using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/UI/States/EmptyState")]
	[DisallowMultipleComponent]
	public class EmptyState : UIState
	{
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        internal override void OnSetActive()
        {
            base.OnSetActive();

            if (Utils.UnityEditor())
                Debug.LogWarning("Empty State => Switched to empty state. Make sure this was intentional.");
        }

        #endregion
    }
}