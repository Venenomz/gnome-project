using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/UI/States/BookOfTheDead")]
	[DisallowMultipleComponent]
	public class BookOfTheDead : UIState
	{
        #region Reference Fields
        [SerializeField, FoldoutGroup("References")] private GameObject _deathUIObject;

        [SerializeField, FoldoutGroup("References")] private Transform _contentHolder;

        private bool _populatedBook = false;

        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        internal override void OnSetActive()
        {
            base.OnSetActive();

            if (!_populatedBook)
                PopulateDeathBook();
        }

        internal void PopulateDeathBook()
        {
            for (int i = 0; i < Game.DeadGnomes.Count; i++)
            {
                GnomeData currentGnome = Game.DeadGnomes[i];

                var created = Instantiate(_deathUIObject, _contentHolder);
            }

            _populatedBook = true;
        }

        internal override void ClosePanel()
        {
            base.ClosePanel();

            Game.UIManager.SwitchMenuState("EndScreen");
        }

        #endregion
    }
}