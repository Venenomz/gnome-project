using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/UI/States/EndScreen")]
	[DisallowMultipleComponent]
	public class EndScreen : UIState
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		public void NewGame()
        {
			// Restart the experience
			Game.Sceneloader.ResetGame();
        }

		public void ShowDeadGnomes()
        {
			Game.UIManager.SwitchMenuState("Book");
        }

		#endregion
	}
}