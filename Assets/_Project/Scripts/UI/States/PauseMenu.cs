using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/UI/States/PauseMenu")]
	[DisallowMultipleComponent]
	public class PauseMenu : UIState
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		internal override void ClosePanel()
		{
			base.ClosePanel();

			Game.UIManager.SwitchMenuState("None");
		}

		#endregion
	}
}