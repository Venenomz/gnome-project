using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/UI/States/HomeScreen")]
	[DisallowMultipleComponent]
	public class HomeScreen : UIState
	{
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        public void Begin()
        {
            Game.UIManager.SwitchMenuState("MainMenu");
        }

        internal override void ClosePanel()
        {
            base.ClosePanel();

            Application.Quit();
        }

        #endregion
    }
}