using UnityEngine;
using UnityEngine.SceneManagement;
using LobsterDog;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/Game/SceneLoader")]
	[DisallowMultipleComponent]
	public class SceneLoader : Singleton<SceneLoader>
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		private const string GAME_SCENE = "Main";

		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		internal void ResetGame()
        {
			Game.Instance.RunCleanUp();

			SceneManager.LoadScene(GAME_SCENE);
        }

		internal void LoadScene(string sceneToLoad)
        {
			SceneManager.LoadScene(sceneToLoad);
		}

		internal void LoadScene(string sceneToLoad, LoadSceneMode mode)
        {
			SceneManager.LoadScene(sceneToLoad, mode);
        }

		internal Scene GetSceneByName(string sceneName)
        {
			return SceneManager.GetSceneByName(sceneName);
        }

		internal void SetActiveScene(Scene newActive)
        {
			SceneManager.SetActiveScene(newActive);
        }

		#endregion
	}
}