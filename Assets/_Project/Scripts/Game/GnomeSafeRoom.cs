using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/Game/GnomeSafeRoom")]
	[DisallowMultipleComponent]
	public class GnomeSafeRoom : MonoBehaviour
	{
		#region Reference Fields

		[SerializeField, FoldoutGroup("References")]  private Transform _defaultSpawnPosition;

		#endregion

		#region Fields
		#endregion

		#region Properties

		internal void Init()
        {

        }

		internal Transform DefaultSpawn
        {
			get { return _defaultSpawnPosition; }
        }

		#endregion

		#region Methods
		#endregion
	}
}