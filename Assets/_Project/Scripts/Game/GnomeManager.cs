using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/Game/GnomeManager")]
	[DisallowMultipleComponent]
	public class GnomeManager : Singleton<GnomeManager>
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		private List<AI_GnomeBehaviour> _gnomesActive;

		#endregion

		#region Fields
		#endregion

		#region Properties

		internal List<AI_GnomeBehaviour> ActiveGnomes
		{
			get { return Instance._gnomesActive; }
		}

		#endregion

		#region Methods
		#endregion
	}
}