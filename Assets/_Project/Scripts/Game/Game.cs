using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using LobsterDog.MapGeneration.DungeonGenerator;

namespace GnomeProject
{
    [RequireComponent(typeof(SceneLoader))]
    public partial class Game : LobsterDogBuildGame
    {
        #region Singleton

        private static Game s_instance;

        public static Game Instance
        {
            get { return s_instance; }
        }

        #endregion

        #region References

        [System.NonSerialized]
        private UIManager _uiManager = null;

        [System.NonSerialized]
        private SceneLoader _sceneLoader = null;

        [System.NonSerialized]
        private InputManager _inputManager = null;

        [System.NonSerialized]
        private GameCamera _gameCamera = null;

        [System.NonSerialized]
        private AI_PathGenerator _pathGenerator = null;

        [System.NonSerialized]
        private AI_PathDataManager _pathDataManager = null;

        [System.NonSerialized]
        private AI_PathFinding _pathFinder = null;

        [System.NonSerialized]
        private HouseCreator _houseCreator = null;

        [System.NonSerialized]
        private GnomeGenerator _gnomeGenerator = null;

        [System.NonSerialized]
        private DetectableTargetManager _detectableTargetManager;

        [System.NonSerialized]
        private GnomeSafeRoom _gnomeSafeZone = null;

        [System.NonSerialized]
        private List<GnomeData> _deadGnomes = null;

        [System.NonSerialized]
        private List<GnomeData> _livingGnomes = null;

        private const string MAP_SCENE = "Map";

        #endregion

        #region Properties

        internal static UIManager UIManager
        {
            get { return s_instance._uiManager; }
        }

        internal static SceneLoader Sceneloader
        {
            get { return s_instance._sceneLoader; }
        }

        internal static AI_PathFinding AIPathFinder
        {
            get { return s_instance._pathFinder; }
        }

        internal static DetectableTargetManager DetectableTargetManager
        {
            get { return s_instance._detectableTargetManager; }
        }

        internal static List<GnomeData> LivingGnomes
        {
            get { return s_instance._livingGnomes; }
        }

        internal static AI_PathDataManager PathDataManager
        {
            get { return s_instance._pathDataManager; }
        }

        internal static List<GnomeData> DeadGnomes
        {
            get { return s_instance._deadGnomes; }
        }

        internal static InputManager InputManager
        {
            get { return s_instance._inputManager; }
        }

        internal static GameCamera GameCamera
        {
            get { return s_instance._gameCamera; }
        }

        internal static AI_PathGenerator PathGenerator
        {
            get { return s_instance._pathGenerator; }
        }

        internal static HouseCreator RoomGenerator
        {
            get { return s_instance._houseCreator; }
        }

        internal static GnomeGenerator GnomeGenerator
        {
            get { return s_instance._gnomeGenerator; }
        }

        internal static GnomeSafeRoom GnomeSafeZone
        {
            get { return s_instance._gnomeSafeZone; }
        }

        #endregion

        #region Methods

        private void Awake()
        {
            s_instance = this;

            StartCoroutine(StartUp());
        }

        private IEnumerator StartUp()
        {
            if (Utils.UnityEditor())
                Debug.Log("Game => Starting Up.");

            _sceneLoader = GetComponent<SceneLoader>();

            if (Utils.UnityEditor())
                Debug.Log("Game => Loading Scene Loader.");

            yield return null;

            _uiManager = FindObjectOfType<UIManager>();

            if (_uiManager.NotNull())
            {
                if (Utils.UnityEditor())
                    Debug.Log("Game => Initialising UI Manager.");

                _uiManager.Init();
            }

            yield return null;

            _inputManager = FindObjectOfType<InputManager>();
        
            if (_inputManager.NotNull())
            {
                if (Utils.UnityEditor())
                    Debug.Log("Game => Initialising Input Manager.");

                _inputManager.Init();
            }

            yield return null;

            _gameCamera = FindObjectOfType<GameCamera>();          

            if (_gameCamera.NotNull())
            {
                if (Utils.UnityEditor())
                    Debug.Log("Game => Initialising Game Camera.");

                _gameCamera.Init();
            }

            yield return null;

            if (_sceneLoader.GetSceneByName(MAP_SCENE).IsValid())
            {
                _sceneLoader.LoadScene(MAP_SCENE, UnityEngine.SceneManagement.LoadSceneMode.Additive);

                if (Utils.UnityEditor())
                    Debug.Log("Game => Trying to load scene: " + MAP_SCENE);

                yield return new WaitUntil(() => _sceneLoader.GetSceneByName(MAP_SCENE).isLoaded == true);

                if (Utils.UnityEditor())
                    Debug.Log("Game => Successfully loaded scene. Setting scene active");

                _sceneLoader.SetActiveScene(_sceneLoader.GetSceneByName(MAP_SCENE));

                if (Utils.UnityEditor())
                    Debug.Log("Game => " + MAP_SCENE + " now active.");

                yield return null;
            }

            _pathGenerator = FindObjectOfType<AI_PathGenerator>();           

            if (_pathGenerator.NotNull())
            {
                if (Utils.UnityEditor())
                    Debug.Log("Game => Initialising Path Generator.");

                _pathGenerator.Init();
            }

            yield return null;

            _houseCreator = FindObjectOfType<HouseCreator>();

            if (_houseCreator.NotNull())
            {
                if (Utils.UnityEditor())
                    Debug.Log("Game => Initialising Room Generator.");

                _houseCreator.Init();
            }

            yield return null;

            _gnomeSafeZone = FindObjectOfType<GnomeSafeRoom>();

            if (_gnomeSafeZone.NotNull())
            {
                if (Utils.UnityEditor())
                    Debug.Log("Game => Initialising Gnome Safe Zone.");

                _gnomeSafeZone.Init();
            }

            yield return null;

            _gnomeGenerator = FindObjectOfType<GnomeGenerator>();           

            if (_gnomeGenerator.NotNull())
            {
                if (Utils.UnityEditor())
                    Debug.Log("Game => Initialising Gnome Generator.");

                _livingGnomes = new List<GnomeData>();
                _deadGnomes = new List<GnomeData>();

                _gnomeGenerator.Init();
            }
        }

        internal void RunCleanUp()
        {
            _uiManager = null;
            _inputManager = null;
            _gnomeGenerator = null;
            _gnomeSafeZone = null;
            _pathGenerator = null;
            _gameCamera = null;
            _sceneLoader = null;

            _livingGnomes.Clear();
            _livingGnomes = null;

            _deadGnomes.Clear();
            _deadGnomes = null;

            s_instance = null;
        }

        #endregion
    }
}
