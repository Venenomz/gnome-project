using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/Game/DayNightManager")]
	[DisallowMultipleComponent]
	public class DayNightManager : MonoBehaviour
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods
		#endregion
	}
}