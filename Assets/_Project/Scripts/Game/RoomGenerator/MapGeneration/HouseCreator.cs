﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GnomeProject;
using Sirenix.OdinInspector;

namespace LobsterDog.MapGeneration.DungeonGenerator
{
    public class HouseCreator : MonoBehaviour
    {
        #region Reference Fields

        [SerializeField, FoldoutGroup("House Variables")]
        private int _mapWidth, _mapLength;

        [SerializeField, FoldoutGroup("House Variables")]
        private int _roomMinWidth, _roomMinLength;

        [SerializeField, FoldoutGroup("House Variables")]
        private int _maxIterations;

        [SerializeField, FoldoutGroup("House Variables")]
        private int _corridorWidth;

        [SerializeField, FoldoutGroup("House Objects")]
        private Material _material;

        [SerializeField, FoldoutGroup("House Variables"), Range(0.0f, 0.3f)]
        private float _bottomCornerMod;

        [SerializeField, FoldoutGroup("House Variables"), Range(0.7f, 1.0f)]
        private float _topCornerMod;

        [SerializeField, FoldoutGroup("House Variables"), Range(0, 2)]
        private int _roomOffset;

        [SerializeField, FoldoutGroup("House Objects")]
        private GameObject _verticalWall, _horizontalWall;

        private List<MapNode> _rooms;

        private List<Vector3Int> _verticalDoorPositions;

        private List<Vector3Int> _horizontalDoorPositions;

        private List<Vector3Int> _horizontalWallPositions;

        private List<Vector3Int> _verticalWallPositions;

        private FurnatureGenerator _furnatureGenerator;

        #endregion

        #region Properties

        internal List<MapNode> Rooms { get { return _rooms; } }

        #endregion

        // Start is called before the first frame update
        void Start()
        {
            
        }

        internal void Init()
        {
            if (_furnatureGenerator == null)
                _furnatureGenerator = GetComponent<FurnatureGenerator>();

            CreateHouse();
        }

        public void CreateHouse()
        {
            DestroyAllChildren();

            HouseGenerator generator = new HouseGenerator(_mapWidth, _mapLength);

            _rooms = new List<MapNode>();

            List<MapNode> listOfRooms = generator.CalculateDungeon(_maxIterations, _roomMinWidth, _roomMinLength, _bottomCornerMod,
                _topCornerMod, _roomOffset, _corridorWidth);

            GameObject wallContainer = new GameObject("Wall Container");

            wallContainer.transform.parent = transform;

            _verticalDoorPositions = new List<Vector3Int>();

            _horizontalDoorPositions = new List<Vector3Int>();

            _horizontalWallPositions = new List<Vector3Int>();

            _verticalWallPositions = new List<Vector3Int>();

            for (int i = 0; i < listOfRooms.Count; i++)
                CreateMesh(listOfRooms[i].BottomLeftAreaCorner, listOfRooms[i].TopRightAreaCorner, listOfRooms[i]);

            CreateWalls(wallContainer.gameObject);

            if (Utils.UnityEditor())
            {
                if (_furnatureGenerator == null)
                    _furnatureGenerator = GetComponent<FurnatureGenerator>();
            }

            foreach (var room in _rooms)
                _furnatureGenerator.SpawnFurnature(room);
        }

        private void CreateWalls(GameObject wallParent)
        {
            foreach (var wallPosition in _horizontalWallPositions)
                CreateWall(wallParent, wallPosition, _horizontalWall);

            foreach (var wallPosition in _verticalWallPositions)
                CreateWall(wallParent, wallPosition, _verticalWall);
        }

        private void CreateWall(GameObject wallParent, Vector3Int wallPosition, GameObject wallPrefab)
        {
            Instantiate(wallPrefab, wallPosition, Quaternion.identity, wallParent.transform);
        }

        private void CreateMesh(Vector2 bottomLeftCorner, Vector2 topRightCorner, MapNode room)
        {
            Vector3 bottomLeftV = new Vector3(bottomLeftCorner.x, 0, bottomLeftCorner.y); // width, height, length (5, 0, 5)

            Vector3 bottomRightV = new Vector3(topRightCorner.x, 0, bottomLeftCorner.y);

            Vector3 topLeftV = new Vector3(bottomLeftCorner.x, 0, topRightCorner.y);

            Vector3 topRightV = new Vector3(topRightCorner.x, 0, topRightCorner.y);

            Vector3[] vertices = new Vector3[]
            {
            topLeftV,
            topRightV,
            bottomLeftV,
            bottomRightV
            };

            Vector2[] uvs = new Vector2[vertices.Length];

            for (int i = 0; i < uvs.Length; i++)
                uvs[i] = new Vector2(vertices[i].x, vertices[i].z);

            char saff = 's';

            int[] triangles = new int[]
            {
            0,
            1,
            2,
            2,
            1,
            3
            };

            Mesh mesh = new Mesh();

            mesh.vertices = vertices;

            mesh.uv = uvs;

            mesh.triangles = triangles;

            GameObject houseFloor = null;

            if (room.RoomIdentifier == -1)
            {
                houseFloor = new GameObject("Corridor " + bottomLeftCorner, typeof(MeshFilter), typeof(MeshRenderer));
            }
            else
            {
                houseFloor = new GameObject(room.ConnectedRoom.NotNull() ? room.ConnectedRoom.RoomType.ToString() : "Room " + bottomLeftCorner, 
                    typeof(MeshFilter), typeof(MeshRenderer));
            }

            room.PhysicalObject = houseFloor;

            houseFloor.transform.position = Vector3.zero;

            houseFloor.transform.localScale = Vector3.one;

            houseFloor.GetComponent<MeshFilter>().mesh = mesh;

            houseFloor.GetComponent<MeshRenderer>().material = _material;

            houseFloor.transform.parent = transform;

            for (int row = (int)bottomLeftV.x; row < (int)bottomRightV.x; row++)
            {
                Vector3 wallPosition = new Vector3(row, 0, bottomLeftV.z);

                AddWallPositionToList(wallPosition, _horizontalWallPositions, _horizontalDoorPositions);
            }

            for (int row = (int)topLeftV.x; row < (int)topRightCorner.x; row++)
            {
                Vector3 wallPosition = new Vector3(row, 0, topRightV.z);

                AddWallPositionToList(wallPosition, _horizontalWallPositions, _horizontalDoorPositions);
            }

            for (int col = (int)bottomLeftV.z; col < (int)topLeftV.z; col++)
            {
                Vector3 wallPosition = new Vector3(bottomLeftV.x, 0, col);

                AddWallPositionToList(wallPosition, _verticalWallPositions, _verticalDoorPositions);
            }

            for (int col = (int)bottomRightV.z; col < (int)topRightV.z; col++)
            {
                Vector3 wallPosition = new Vector3(bottomRightV.x, 0, col);

                AddWallPositionToList(wallPosition, _verticalWallPositions, _verticalDoorPositions);
            }

            _rooms.Add(room);
        }

        private void AddWallPositionToList(Vector3 wallPosition, List<Vector3Int> wallList, List<Vector3Int> doorList)
        {
            Vector3Int point = Vector3Int.CeilToInt(wallPosition);

            if (wallList.Contains(point))
            {
                doorList.Add(point);
                wallList.Remove(point);
            }
            else
            {
                wallList.Add(point);
            }
        }

        private void DestroyAllChildren()
        {
            while (transform.childCount != 0)
            {
                foreach (Transform item in transform)
                    DestroyImmediate(item.gameObject);
            }
        }
    }
}
