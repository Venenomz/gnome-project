﻿using UnityEngine;

namespace LobsterDog.MapGeneration.DungeonGenerator
{
    public class RoomNode : MapNode
    {
        public RoomNode(Vector2Int bottomLeftAreaCorner, Vector2Int topRightAreaCorner, MapNode parentNode, int index) : base(parentNode)
        {
            this.BottomLeftAreaCorner = bottomLeftAreaCorner;

            this.TopRightAreaCorner = topRightAreaCorner;

            this.BottomRightAreaCorner = new Vector2Int(topRightAreaCorner.x, bottomLeftAreaCorner.y);

            this.TopLeftAreaCorner = new Vector2Int(bottomLeftAreaCorner.x, TopRightAreaCorner.y);

            this.TreeLayerIndex = index;
        }

        //public int Width { get => (int)(TopRightAreaCorner.x - BottomLeftAreaCorner.x); }
        //public int Length { get => (int)(TopRightAreaCorner.y - BottomLeftAreaCorner.y); }
    }
}