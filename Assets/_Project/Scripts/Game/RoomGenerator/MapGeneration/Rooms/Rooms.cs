using Sirenix.OdinInspector;
using UnityEngine;

namespace GnomeProject
{
    [AddComponentMenu("Gnome Project/Game/RoomGenerator/MapGeneration/Rooms")]
	[DisallowMultipleComponent] [CreateAssetMenu()]
	public class Rooms : ScriptableObject
	{
		#region Reference Fields
		[SerializeField, FoldoutGroup("Room Objects")]
		protected GameObject[] _roomFurnature;

		[SerializeField]
		protected TypeOfRoom _roomType;

		internal TypeOfRoom RoomType { get { return _roomType; } }

		#endregion

		#region Fields

		public enum TypeOfRoom
        {
			NONE,
			BEDROOM,
			LIVING_ROOM,
			KITCHEN,
			GARAGE,
			BATHROOM,
			CORRIDOR
        }

		#endregion

		#region Properties

		internal GameObject[] AvailableFurnature { get { return _roomFurnature; } }

		#endregion

		#region Methods

		internal virtual void Init()
        {

        }

		#endregion
	}
}