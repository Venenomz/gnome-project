﻿using System.Collections.Generic;
using GnomeProject;
using UnityEngine;

namespace LobsterDog.MapGeneration.DungeonGenerator
{
    public abstract class MapNode
    {
        private List<MapNode> _childNodes;

        public List<MapNode> ChildrenNodeList { get => _childNodes; }
        public GameObject PhysicalObject { get; set; }
        public bool Visted { get; set; }
        public Vector2Int BottomLeftAreaCorner { get; set; }
        public Vector2Int BottomRightAreaCorner { get; set; }
        public Vector2Int TopRightAreaCorner { get; set; }
        public Vector2Int TopLeftAreaCorner { get; set; }
        public Rooms ConnectedRoom { get; set; }
        public MapNode Parent { get; set; }

        public int RoomIdentifier { get; set; }

        public int Width { get => (int)(TopRightAreaCorner.x - BottomLeftAreaCorner.x); }
        public int Length { get => (int)(TopRightAreaCorner.y - BottomLeftAreaCorner.y); }

        public int TreeLayerIndex { get; set; }

        public MapNode(MapNode parentNode)
        {
            _childNodes = new List<MapNode>();

            this.Parent = parentNode;

            if (parentNode.NotNull())
                parentNode.AddChild(this);
        }

        public void AddChild(MapNode node)
        {
            _childNodes.Add(node);
        }

        public void RemoveChild(MapNode node)
        {
            _childNodes.Remove(node);
        }
    }
}