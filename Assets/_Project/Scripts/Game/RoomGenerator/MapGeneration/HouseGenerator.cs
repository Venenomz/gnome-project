﻿using GnomeProject;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LobsterDog.MapGeneration.DungeonGenerator
{
    public class HouseGenerator
    {
        List<RoomNode> _allNodes = new List<RoomNode>();

        private List<Rooms> _roomDatas;

        private int _dungeonWidth;
        private int _dungeonLength;

        int _bedRoomMin = 1;

        public HouseGenerator(int dungeonWidth, int dungeonLength)
        {
            this._dungeonWidth = dungeonWidth;
            this._dungeonLength = dungeonLength;
        }

        public List<MapNode> CalculateDungeon(int maxIterations, int roomWidthMin, int roomLengthMin, float roomBottomCornerModifier, float roomTopCornerMidifier, int roomOffset, int corridorWidth)
        {
            BinarySpacePartitioner bsp = new BinarySpacePartitioner(_dungeonWidth, _dungeonLength);
            _allNodes = bsp.PrepareNodesCollection(maxIterations, roomWidthMin, roomLengthMin);
            List<MapNode> roomSpaces = StructureHelper.TraverseGraphToExtractLowestLeafes(bsp.RootNode);

            RoomGenerator roomGenerator = new RoomGenerator(maxIterations, roomLengthMin, roomWidthMin);
            List<RoomNode> roomList = roomGenerator.GenerateRoomsInGivenSpaces(roomSpaces, roomBottomCornerModifier, roomTopCornerMidifier, roomOffset);

            LoadRoomData();

            for (int i = 0; i < roomList.Count; i++)
            {
                if (i == 0)
                {
                    roomList[i].ConnectedRoom = _roomDatas[2];

                    _roomDatas.Remove(_roomDatas[2]);

                    continue;
                }

                int randRoom = UnityEngine.Random.Range(0, _roomDatas.Count - 1);

                if (_roomDatas.InRange(randRoom))
                {
                    roomList[i].ConnectedRoom = _roomDatas[randRoom];

                    if (_roomDatas[randRoom].RoomType == Rooms.TypeOfRoom.BEDROOM && _bedRoomMin > 0)
                    {
                        _bedRoomMin--;

                        continue;
                    }

                    _roomDatas.Remove(_roomDatas[randRoom]);
                }
            }

            CorridorsGenerator corridorGenerator = new CorridorsGenerator();
            List<MapNode> corridorList = corridorGenerator.CreateCorridor(_allNodes, corridorWidth);

            for (int i = 0; i < corridorList.Count; i++)
                corridorList[i].RoomIdentifier = -1;

            return new List<MapNode>(roomList).Concat(corridorList).ToList();
        }

        private void LoadRoomData()
        {
            _roomDatas = new List<Rooms>();

            Rooms[] roomArray = Resources.LoadAll<Rooms>("Rooms/");

            for (int i = 0; i < roomArray.Length; i++)
                _roomDatas.Add(roomArray[i]);
        }
    }
}