﻿using UnityEngine;

namespace LobsterDog.MapGeneration.DungeonGenerator
{
    public class MapLine
    {
        E_Orientation _orientation;

        Vector2Int _coords;

        public MapLine(E_Orientation orientation, Vector2Int coordinates)
        {
            this._orientation = orientation;
            this._coords = coordinates;
        }

        public E_Orientation Orientation { get => _orientation; set => _orientation = value; }
        public Vector2Int Coordinates { get => _coords; set => _coords = value; }
    }

    public enum E_Orientation
    {
        HORIZONTAL,
        VERTICAL
    }
}