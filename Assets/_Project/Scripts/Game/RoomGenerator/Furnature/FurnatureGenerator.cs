using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using LobsterDog.MapGeneration.DungeonGenerator;
using Sirenix.OdinInspector;
using Random = UnityEngine.Random;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/Game/RoomGenerator/Furnature/FurnatureGenerator")]
	[DisallowMultipleComponent]
	public class FurnatureGenerator : Singleton<FurnatureGenerator>
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]
		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		internal void SpawnFurnature(MapNode node)
        {
			if (node.ConnectedRoom.NotNull() && node.ConnectedRoom.AvailableFurnature.NotNullOrEmpty())
			{
				for (int i = 0; i < node.ConnectedRoom.AvailableFurnature.Length; i++)
				{
					GameObject toSpawn = Instantiate(node.ConnectedRoom.AvailableFurnature[i], Vector3.zero,
						Quaternion.identity, node.PhysicalObject.transform);

					Vector3 randomPosition = new Vector3(Random.Range(-node.Width, node.Width), -1, Random.Range(-node.Length, node.Length))
					+ node.PhysicalObject.transform.position;

					toSpawn.name = node.ConnectedRoom.AvailableFurnature[i].name;					
					toSpawn.transform.position = randomPosition;
					toSpawn.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
				}
			}
        }

		#endregion
	}
}