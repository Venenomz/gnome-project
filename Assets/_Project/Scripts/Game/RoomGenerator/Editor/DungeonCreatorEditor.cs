﻿using UnityEditor;
using UnityEngine;

namespace LobsterDog.MapGeneration.DungeonGenerator
{
    [CustomEditor(typeof(HouseCreator))]
    public class HouseCreatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            HouseCreator houseCreator = (HouseCreator)target;

            if (GUILayout.Button("Create New House"))
                houseCreator.CreateHouse();
        }
    }
}
