using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/Game/GnomeGenerator")]
	[DisallowMultipleComponent]
	public class GnomeGenerator : Singleton<GnomeGenerator>
	{
		#region Reference Fields
		[SerializeField, FoldoutGroup("References")] private AI_GnomeBehaviour _gnomeObject;

		Traits[] _allTraits;

		private int _amountToSpawn;

		private int _currentGnomes;

		private const int MAX_GNOMES = 6;

		private const int CHANGE_TO_GET_TRAIT = 15;

		private const int INHERITANCE_CHANCE = 5;

		private const string TRAIT_PATH = "Assets/_Project/Resources/Traits";

		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		private bool CanSpawnGnomes()
		{
			return _currentGnomes == MAX_GNOMES;
		}

		internal void Init()
		{
			_allTraits = Resources.LoadAll(TRAIT_PATH) as Traits[];

			_amountToSpawn = MAX_GNOMES;

			GenerateGnome();
		}

		internal void GenerateGnome(GnomeData parent1, GnomeData parent2)
		{
			if (parent1.IsNull() || parent2.IsNull())
				return;

			_amountToSpawn = _currentGnomes % MAX_GNOMES;

			for (int i = 0; i < _amountToSpawn; i++)
			{
				if (!CanSpawnGnomes() || _gnomeObject.IsNull())
					return;

				GnomeData newData = ScriptableObject.CreateInstance("GnomeData") as GnomeData;

				newData.SetData(GenerateName(), 0, GenerateTraits());

				AquireTraitsFromParents(parent1, parent2, newData);

				AI_GnomeBehaviour created = Instantiate(_gnomeObject, Game.GnomeSafeZone.DefaultSpawn.position, Quaternion.identity);

				created.SetData(newData);

				created.Init();

				Game.LivingGnomes.Add(newData);

				UpdateCurrentGnomeCounter(false);
			}
		}

		internal void GenerateGnome()
		{
			for (int i = 0; i < _amountToSpawn; i++)
			{
				if (!CanSpawnGnomes() || _gnomeObject.IsNull())
					return;

				GnomeData newData = ScriptableObject.CreateInstance("GnomeData") as GnomeData;

				newData.SetData(GenerateName(), 0, GenerateTraits());

				AI_GnomeBehaviour created = Instantiate(_gnomeObject, Game.GnomeSafeZone.DefaultSpawn.position, Quaternion.identity);

				created.SetData(newData);

				created.Init();

				Game.LivingGnomes.Add(newData);

				UpdateCurrentGnomeCounter(false);
			}
		}

		internal void UpdateCurrentGnomeCounter(bool isDead)
		{
			_currentGnomes = isDead ? _currentGnomes -= 1 : _currentGnomes += 1;
		}

		private List<Traits> GenerateTraits()
		{
			List<Traits> newTraitList = new List<Traits>();

			for (int i = 0; i < _allTraits.Length; i++)
			{
				Traits currentTrait = _allTraits[i];

				int randValue = UnityEngine.Random.Range(0, 100);

				if (randValue <= CHANGE_TO_GET_TRAIT)
				{
					newTraitList.Add(currentTrait);
				}
			}

			return newTraitList;
		}

		private string GenerateName()
        {
			return "Test Name";
        }

		private void AquireTraitsFromParents(GnomeData parent1, GnomeData parent2, GnomeData child)
        {
            for (int x = 0; x < parent1.Traits.Count; x++)
            {
				Traits p1Current = parent1.Traits[x];

				int randomValue = UnityEngine.Random.Range(0, 100);

				if (randomValue <= INHERITANCE_CHANCE)
				{
					if (!child.Traits.Contains(p1Current))
						child.Traits.Add(p1Current);
				}
			}

			for (int y = 0; y < parent2.Traits.Count; y++)
			{
				Traits p2Current = parent2.Traits[y];

				int randomValue = UnityEngine.Random.Range(0, 100);

				if (randomValue <= INHERITANCE_CHANCE)
				{
					if (!child.Traits.Contains(p2Current))
						child.Traits.Add(p2Current);
				}
			}
		}

		#endregion
	}
}