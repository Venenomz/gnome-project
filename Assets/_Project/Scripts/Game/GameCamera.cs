using System.Collections;
using System.Collections.Generic;
using LobsterDog;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GnomeProject
{
    public class GameCamera : Singleton<GameCamera>
    {
        #region Variables

        private Camera _camera = null;

        private ICameraTrackable _target = null;

        private Vector2 _movementVelocity = Vector2.zero;

        private Vector2 _swipeMoveSensitivity;

        private const float MAX_MOVE_VELOCITY = 5f;

        private Vector2 _startMovePosition;

        private Vector2 _endMovePosition;

        private Vector2 _targetOffset = Vector3.zero;

        private float _totalMovementTimeInSeconds = 1f;

        private float _movementSpeed = 2f;

        private float _transitionTimer;

        [SerializeField, ReadOnly]
        private float _movementTimeRemaining;

        [SerializeField]
        private AnimationCurve _movementCurve;

        private bool _inited = false;
        private bool _onTarget = false;

        #endregion

        #region Properties

        internal Camera Cam
        {
            get { return Instance._camera ?? Camera.main; }
        }

        internal enum TransitionStyle
        {
            LINEAR,
            EASE_IN_OUT
        }

        internal Vector3 CameraPosition
        {
            get { return Instance._camera == null ? Vector3.zero : _camera.transform.position; }
        }

        internal Vector3 ControllerPosition
        {
            get { return Instance.transform.position; }
        }

        #endregion

        #region Methods

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_inited)
            {
                HandleMovement();

                if (_target != null)
                {
                    UpdateTargetedFocus();
                    HandleTargetedMovement();
                }
            }
        }

        internal void Init()
        {
            _camera = Camera.main;

            _inited = true;
        }

        internal void ReleaseTarget(ICameraTrackable targ)
        {
            if (targ == _target)
            {
                _target = null;

                _onTarget = false;

                _movementVelocity = Vector2.zero;

                StartTargetedLerp(new Vector3(transform.position.x, transform.position.y));
            }
        }

        internal void JumpToTargetImmediately()
        {
            if (_target.NotNull())
                SetTarget(_target, 0, 0, 0, false);
        }

        private void UpdateTargetedFocus()
        {
            if (_target.NotNull())
            {
                if (_transitionTimer > 0f)
                {
                    Vector3 targetPositon = new Vector3(_targetOffset.x + _target.Position.x, _targetOffset.y + _target.Position.y);

                    _transitionTimer -= Time.deltaTime;

                    StartTargetedLerp(targetPositon);
                }
                else
                {
                    _onTarget = true;

                    // TODO: Display UI
                }
            }
        }

        internal void SetTarget(ICameraTrackable target, float offsetX = 0f, float offsetY = 0f, float transitionSeconds = 0f, bool softEase = false)
        {
            if (target.NotNull())
            {
                _target = target;
                _targetOffset = new Vector2(offsetX, offsetY);

                _transitionTimer = transitionSeconds;

                if (transitionSeconds == 0f)
                {
                    if (softEase == false)
                    {
                        transform.position = _target.Position + _targetOffset;

                        _onTarget = true;
                    }
                }
                else
                {
                    _startMovePosition = transform.position;
                    _movementTimeRemaining = _totalMovementTimeInSeconds;
                }
            }
        }

        private void StartTargetedLerp(Vector2 targetPos)
        {
            _startMovePosition = transform.position;
            _endMovePosition = targetPos;
            _movementTimeRemaining = _totalMovementTimeInSeconds;
        }

        private void HandleTargetedMovement()
        {
            if (_endMovePosition != Vector2.zero)
            {
                float t = _movementCurve.Evaluate(_totalMovementTimeInSeconds - _movementTimeRemaining);

                _movementTimeRemaining -= Time.deltaTime;

                transform.position = Vector3.Lerp(_startMovePosition, _endMovePosition, t);
            }
        }

        private void HandleMovement()
        {
            if (_movementVelocity != Vector2.zero)
            {
                _movementVelocity = Vector2.MoveTowards(_movementVelocity, Vector2.zero, _movementSpeed * Time.deltaTime);

                transform.position -= -_movementVelocity.ToVector3() * Time.deltaTime;
            }
        }

        internal void OnSwipe(Vector2 delta)
        {
            if (delta.magnitude == 0)
            {
                _movementVelocity = Vector2.zero;
            }
            else
            {
                if (_target != null)
                    ExitInteractionViaMovement();

                _movementVelocity = delta * _swipeMoveSensitivity;

                if (_movementVelocity.magnitude > MAX_MOVE_VELOCITY)
                    _movementVelocity = _movementVelocity.normalized * MAX_MOVE_VELOCITY;
            }
        }

        private void ExitInteractionViaMovement()
        {
            // TODO: Exit Code
        }

        private void SwitchToTrackingCam()
        {

        }

        #endregion
    }

    public interface ICameraTrackable
    {
        Vector2 Position
        {
            get;
        }
    }

    public class CameraTarget : ICameraTrackable
    {
        Vector2 m_pos;

        public Vector2 Position
        {
            get { return m_pos; }
        }

        internal void SetPosition(Vector3 pos)
        {
            m_pos = pos;
        }

        internal void SetPositionX(float pos)
        {
            m_pos.x = pos;
        }

        internal void SetPositionY(float pos)
        {
            m_pos.y = pos;
        }

        public void Translate(Vector2 delta)
        {
            m_pos += delta;
        }
    }
}
