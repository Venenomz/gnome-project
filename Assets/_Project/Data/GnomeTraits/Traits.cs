using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/Traits")]
	[DisallowMultipleComponent]
	public class Traits : ScriptableObject
	{
		#region Reference Fields
		[SerializeField, FoldoutGroup("References")]
		protected string _traitID;

		protected string _traitDescription;

		protected Sprite _traitIcon;

		#endregion

		#region Fields
		#endregion

		#region Properties
		#endregion

		#region Methods

		internal virtual void ApplyTraitModifiers(AI_GnomeBehaviour targetGnome)
        {

        }

		#endregion
	}
}