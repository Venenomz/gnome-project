using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/GnomeData")]
	[DisallowMultipleComponent]
	public class GnomeData : ScriptableObject
	{
		#region Reference Fields
		//[SerializeField, FoldoutGroup("References")]

		private string _name;
		private int _age;
		private List<Traits> _traits;

		private DateTime _deathTime;

		#endregion

		#region Fields
		#endregion

		#region Properties

		internal DateTime TimeOfDeath
        {
			get { return _deathTime; }
			private set { _deathTime = value; }
        }

		internal string GnomeName
        {
            get { return _name; }
        }

		internal int Age
        {
			get { return _age; }
        }

		internal List<Traits> Traits
        {
            get { return _traits; }
        }

		#endregion

		#region Methods

		internal void SetData(string gName, int age, List<Traits> traits)
        {
			_name = gName;
			_age = age;
			_traits = traits;
        }

		internal void AddAge()
        {
			_age += 1;
        }

		internal void SetDeathTime()
        {
			TimeOfDeath = DateTime.Now;
        }

		#endregion
	}
}