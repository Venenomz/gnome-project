using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LobsterDog;
using Sirenix.OdinInspector;

namespace GnomeProject
{
	[AddComponentMenu("Gnome Project/ButterFingers")]
	[DisallowMultipleComponent]
	public class ButterFingers : Traits
	{
        #region Reference Fields
        //[SerializeField, FoldoutGroup("References")]
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Methods

        internal override void ApplyTraitModifiers(AI_GnomeBehaviour targetGnome)
        {
            base.ApplyTraitModifiers(targetGnome);


        }

        #endregion
    }
}